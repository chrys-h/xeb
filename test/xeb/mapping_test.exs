defmodule XEB.MappingTest do
  use ExUnit.Case
  import XEB.Mapping

  describe "lookup_mod_reverse" do
    test "recognizes key names" do
      assert lookup_mod_reverse(:Shift_L) == :Shift
    end

    test "recognizes keysyms" do
      keysym = %XEB.Proto.Core.KEYSYM{val: 0xffe1}
      assert lookup_mod_reverse(keysym) == :Shift
    end
  end
end
