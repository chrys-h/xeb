defmodule XEB.Identify.TestHelper do
  # This is just syntactic sugar to make long hexadecimal numbers less tedious
  # to write. 'h' stands for hexadecimal.
  def do_sigil_h("") do
    ""
  end

  def do_sigil_h(<<_::size(8)>>) do
    raise ArgumentError, message: "Incorrect hexadecimal"
  end

  def do_sigil_h(<<c1::size(8), c2::size(8), rest::binary>>) do
    {x, ""} = Integer.parse(<<c1::size(8), c2::size(8)>>, 16)
    <<x::size(8)>> <> do_sigil_h(rest)
  end

  # Hexadecimal number of arbitrary length as a binary string of bytes
  def sigil_h(str, []) do
    str
    |> String.upcase
    |> to_charlist
    # Drop non-numeric characters
    |> Enum.filter(&((&1 >= 48 && &1 <= 57) || (&1 >= 65 && &1 <= 70)))
    |> to_string
    |> do_sigil_h
  end

  # Unsigned 32-bit ints of native endiannes given as hexadecimal numbers
  # and converted to decimal; will fail if more or less than 4 bytes are
  # given
  def sigil_h(str, 'i') do
    <<x::unsigned-native-32>> = sigil_h(str, [])
    x
  end
end

defmodule XEB.IdentifyTest do
  use ExUnit.Case
  import XEB.Identify.TestHelper

  describe "XEB.Identify.identify/2" do
    test "identifies replies" do
      raw = ~h(0100df0001000000 0400000000000000 0000000000000000 0000000000000000
               42617365)
      iden = {:reply, %{seq_num: 223, len: 1}}

      assert XEB.Identify.identify(raw) == iden
    end

    test "identifies core events" do
      raw = ~h(1c00eb02be040000 57010000789bda01 0000000000000000 0000000000000000)
      iden = {:event, %{ext: "Core", name: :PropertyNotifyEvent, seq_num: 747}}

      assert XEB.Identify.identify(raw) == iden
    end

   #test "identifies non-core events" do
   #  raw = ~h(560069e10100a000 97b6a00001000000 77e7df0176e7df01 0000000000000000)
   #  iden = {:event, %{ext: :xfixes, name: :SelectionNotifyEvent, seq_num: 57705}}
   #  exts = %{events: [xproto: 0, xfixes: 86],
   #           errors: [xproto: 0, xfixes: 138],
   #           opcodes: [xfixes: 137]}

   #  assert XEB.Identify.identify(raw, exts) == iden
   #end

    test "identifies core errors" do
      raw = ~h(000a4500be040000 0100820000000000 0000000000000000 0000000000000000)
      iden = {:error, %{ext: "Core", name: :AccessError, seq_num: 69,
                       min_opcode: 1, maj_opcode: 130}}

      assert XEB.Identify.identify(raw) == iden
    end

   #@tag :skip # This will have to wait until Shm is supported...
   #test "identifies non-core errors" do
   #  raw = ~h(00804600be040000 0200820000000000 0000000000000000 0000000000000000)
   #  iden = {:error, %{ext: :shm, name: :BadSegError, seq_num: 70,
   #                   min_opcode: 2, maj_opcode: 130}}
   #  exts = %{events: [xproto: 0, shm: 65],
   #           errors: [xproto: 0, shm: 128],
   #           opcodes: [shm: 130]}

   #  assert XEB.Identify.identify(raw, exts) == iden
   #end

    test "fails with bad input" do
      assert XEB.Identify.identify("Invalid data") == :bad_data
    end

    test "identifies lists of events/errors" do
      raw = ~h(1c005e02be040000 5701000044aada01 0000000000000000 0000000000000000
               09035e028200a000 0000000000000000 0000000000000000 0000000000000000
               1c005e02be040000 5701000044aada01 0000000000000000 0000000000000000
               1c005e028200a000 4f01000044aada01 0000000000000000 0000000000000000)
      iden = [{:event, %{ext: "Core", name: :PropertyNotifyEvent, seq_num: 606}},
              {:event, %{ext: "Core", name: :FocusInEvent, seq_num: 606}},
              {:event, %{ext: "Core", name: :PropertyNotifyEvent, seq_num: 606}},
              {:event, %{ext: "Core", name: :PropertyNotifyEvent, seq_num: 606}}]

      assert XEB.Identify.identify(raw) == iden
    end
  end
end

