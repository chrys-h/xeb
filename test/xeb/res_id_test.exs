defmodule XEB.ResIDTest do
  use ExUnit.Case
  use Bitwise

  setup do
    Process.flag :trap_exit, true
    {:ok, server} = XEB.ResID.start_link
    range_mask = (:math.pow(2, 19) |> round) - 1
    range_base = 1 <<< 21
    XEB.ResID.set_range server, range_mask, range_base

    {:ok, server: server, mask: range_mask, base: range_base}
  end

  describe "Allocation of new XIDs" do
    test "works", %{server: svr, base: b} do
      xid = XEB.ResID.id_alloc svr
      assert xid >= b
    end

    test "causes the XID to be owned by the allocator", %{server: svr} do
      curr = self()
      allocator = spawn fn ->
        xid = XEB.ResID.id_alloc svr
        send curr, {:xid, xid}
        receive do :end -> exit(:ended) end
      end

      xid = receive do {:xid, i} -> i end

      owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(owners, allocator)

      send allocator, :end
    end
  end

  describe "Release of XIDs" do
    test "stops the XID from being owned by the requesting process", %{server: svr} do
      curr = self()
      allocator = spawn fn ->
        xid = XEB.ResID.id_alloc svr
        send curr, {:xid, xid}

        receive do :do_free ->
          XEB.ResID.id_release svr, xid
          send curr, :go_ahead
        end
        receive do :end -> exit(:ended) end
      end

      xid = receive do {:xid, i} -> i end

      owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(owners, allocator)

      send allocator, :do_free
      receive do :go_ahead -> nil end

      new_owners = XEB.ResID.who_has svr, xid
      refute MapSet.member?(new_owners, allocator)

      send allocator, :end
    end

    test "is ignored if the requesting process is not an owner of the XID", %{server: svr} do
      curr = self()
      allocator = spawn fn ->
        xid = XEB.ResID.id_alloc svr
        send curr, {:xid, xid}

        receive do :end -> exit(:ended) end
      end

      xid = receive do {:xid, i} -> i end

      owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(owners, allocator)

      XEB.ResID.id_release svr, xid

      new_owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(new_owners, allocator)

      send allocator, :end
    end
  end

  describe "Ownership of XIDs" do
    test "is not retained after owner dies", %{server: svr} do
      curr = self()
      allocator = spawn_link fn ->
        xid = XEB.ResID.id_alloc svr
        send curr, {:xid, xid}
        receive do :end -> exit(:ended) end
      end

      xid = receive do {:xid, i} -> i end

      owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(owners, allocator)

      send allocator, :end
      receive do {:EXIT, ^allocator, :ended} -> nil end

      new_owners = XEB.ResID.who_has svr, xid
      refute MapSet.member?(new_owners, allocator)
    end

    test "query returns the empty set if the XID has no owner", %{server: svr} do
      owners = XEB.ResID.who_has svr, 100
      assert Enum.empty?(owners)
    end
  end

  describe "Ownership seizure" do
    test "works in normal conditions", %{server: svr} do
      curr = self()
      xid = 223
      XEB.ResID.id_seize svr, xid

      owners = XEB.ResID.who_has svr, xid
      assert MapSet.member?(owners, curr)
    end

    test "fails if the requested XID is invalid", %{server: svr} do
      assert_raise RuntimeError, "Trying to seize an invalid XID", fn ->
        XEB.ResID.id_seize svr, 1 <<< 31
      end
    end
  end
end

