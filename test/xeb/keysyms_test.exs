defmodule XEB.KeysymsTest do
  use ExUnit.Case

  describe "Keysym lookup function" do
    test "recognizes characters" do
      assert XEB.Keysyms.lookup_name(:a) == %XEB.Proto.Core.KEYSYM{val: 97}
    end

    test "recognizes control keys" do
      assert XEB.Keysyms.lookup_name(:Enter) == %XEB.Proto.Core.KEYSYM{val: 0xff0d}
    end
  end
end

