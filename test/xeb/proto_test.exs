defmodule XEB.Proto.TestHelper do
  # This is just syntactic sugar to make long hexadecimal numbers less tedious
  # to write. 'h' stands for hexadecimal.
  def do_sigil_h("") do
    ""
  end

  def do_sigil_h(<<_::size(8)>>) do
    raise ArgumentError, message: "Incorrect hexadecimal"
  end

  def do_sigil_h(<<c1::size(8), c2::size(8), rest::binary>>) do
    {x, ""} = Integer.parse(<<c1::size(8), c2::size(8)>>, 16)
    <<x::size(8)>> <> do_sigil_h(rest)
  end

  # Hexadecimal number of arbitrary length as a binary string of bytes
  def sigil_h(str, []) do
    str
    |> String.upcase
    |> to_charlist
    # Drop non-numeric characters
    |> Enum.filter(&((&1 >= 48 && &1 <= 57) || (&1 >= 65 && &1 <= 70)))
    |> to_string
    |> do_sigil_h
  end

  # Unsigned 32-bit ints of native endiannes given as hexadecimal numbers
  # and converted to decimal; will fail if more or less than 4 bytes are
  # given
  def sigil_h(str, 'i') do
    <<x::unsigned-native-32>> = sigil_h(str, [])
    x
  end
end


defmodule XEB.ProtoTest do
  use ExUnit.Case
  import XEB.Proto.TestHelper

  describe "Structs" do
    @bin ~h(20ff1d009d061d00)
    @elx %XEB.Proto.Core.SEGMENT{x1: -224, y1: 29, x2: 1693, y2: 29}

    test "are correctly parsed" do
      assert XEB.Proto.Core.SEGMENT.from_X(@bin) == @elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.SEGMENT.to_X(@elx) == @bin
    end
  end

  describe "Unions" do
    @bin ~h(6e65773a2049443d 69332f6669726566 6f782f31)
    @elx %XEB.Proto.Core.ClientMessageData{val: "new: ID=i3/firefox/1"}

    test "are correctly parsed" do
      assert XEB.Proto.Core.ClientMessageData.from_X(@bin) == @elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.ClientMessageData.to_X(@elx) == @bin
    end
  end

  describe "XID types" do
    @bin ~h(be040000)
    @elx %XEB.Proto.Core.WINDOW{xid: ~h(be040000)i}

    test "are correctly parsed" do
      assert XEB.Proto.Core.WINDOW.from_X(@bin) == @elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.WINDOW.to_X(@elx) == @bin
    end
  end

  describe "XID unions" do
    @drawable_bin ~h(0f004000)
    @drawable_elx %XEB.Proto.Core.DRAWABLE{xid: ~h(0f004000)i}

    @window_bin ~h(0f004000)
    @window_elx %XEB.Proto.Core.WINDOW{xid: ~h(0f004000)i}

    test "are correctly parsed" do
      assert XEB.Proto.Core.DRAWABLE.from_X(@drawable_bin) == @drawable_elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.DRAWABLE.to_X(@window_elx) == @window_bin
    end
  end

  describe "Enums" do
    @int 2
    @elx :ZPixmap

    test "are correctly parsed" do
      assert XEB.Proto.Core.ImageFormat.from_X(@int) == @elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.ImageFormat.to_X(@elx) == @int
    end
  end

  describe "Masks" do
    @int ~h(0c000100)i
    @elx_lst [:Foreground, :Background, :GraphicsExposures]

    test "are correctly parsed" do
      elx_set = MapSet.new @elx_lst
      assert XEB.Proto.Core.GC.from_X(@int) == elx_set
    end

    test "are correctly generated from lists" do
      assert XEB.Proto.Core.GC.to_X(@elx_lst) == @int
    end

    test "are correctly generated from MapSets" do
      elx_set = MapSet.new @elx_lst
      assert XEB.Proto.Core.GC.to_X(elx_set) == @int
    end
  end

  describe "Typedefs" do
    @bin ~h(40)
    @elx %XEB.Proto.Core.KEYCODE{val: 64}

    test "are correctly parsed" do
      assert XEB.Proto.Core.KEYCODE.from_X(@bin) == @elx
    end

    test "are correctly generated" do
      assert XEB.Proto.Core.KEYCODE.to_X(@elx) == @bin
    end
  end

  describe "Requests" do
    @bin ~h(070004008200a000 5900400000000000)
    @elx %XEB.Proto.Core.ReparentWindow{
      window: %XEB.Proto.Core.WINDOW{xid: ~h(8200a000)i},
      parent: %XEB.Proto.Core.WINDOW{xid: ~h(59004000)i},
      x: 0, y: 0
    }

   #test "are correctly parsed" do
   #  Request parsing is not considered useful
   #end

    test "are correctly generated" do
      assert XEB.Proto.Core.ReparentWindow.to_X(@elx) == @bin
    end
  end

 #describe "Non-core requests" do
 #  @exts %{events:  [shape: 42, xproto: 0],
 #          errors:  [shape: 42, xproto: 0],
 #          opcodes: [shape: 129]}

 # #test "are correctly parsed" do
 # #  Request parsing is not considered useful
 # #end

 #  test "are correctly generated" do
 #    bin = ~h(81000100)
 #    elx = %XEB.Proto.Shape.QueryVersion{}
 #    assert XEB.Proto.Shape.QueryVersion.to_X(elx, @exts) == bin
 #  end

 #  test "generation fails when extension is unsupported" do
 #    elx = %XEB.Proto.XFixes.QueryVersion{
 #      client_major_version: 5,
 #      client_minor_version: 0
 #    }
 #    assert XEB.Proto.XFixes.QueryVersion.to_X(elx, @exts) == :unknown_ext
 #  end
 #end

  describe "Replies" do
    @bin ~h(0120110300000000 be040000f1fff1ff 0a000a0000000000 0000000000000000)
    @elx %XEB.Proto.Core.GetGeometry.Reply{
      depth: 32, x: -15, y: -15, width: 10, height: 10, border_width: 0,
      root: %XEB.Proto.Core.WINDOW{xid: ~h(be040000)i}
    }

    test "are correctly parsed" do
      assert XEB.Proto.Core.GetGeometry.Reply.from_X(@bin) == @elx
    end

  # test "are correctly generated" do
  #   Reply generation is not considered useful
  # end
  end

  describe "Events" do
    test "are correctly parsed" do
      bin = ~h(0c0066b68200a000 cc012200a7001b00 0000000000000000 0000000000000000)
      elx = %XEB.Proto.Core.ExposeEvent{
        window: %XEB.Proto.Core.WINDOW{xid: ~h(8200a000)i},
        x: 460, y: 34, width: 167, height: 27, count: 0,
        usersent: false
      }

      assert XEB.Proto.Core.ExposeEvent.from_X(bin) == elx
    end

    test "are correctly generated" do
      bin = ~h(2108000061004000 3601000031205343 5245454e3d30204e 414d453d69332044)
      elx = %XEB.Proto.Core.ClientMessageEvent{
        format: 8, usersent: false,
        window: %XEB.Proto.Core.WINDOW{xid: ~h(61004000)i},
        type: %XEB.Proto.Core.ATOM{xid: 310},
        data: %XEB.Proto.Core.ClientMessageData{
          val: ~h(312053435245454e3d30204e414d453d69332044)}
      }

      assert XEB.Proto.Core.ClientMessageEvent.to_X(elx) == bin
    end
  end

  describe "Events defined as eventcopy" do
    @bin ~h(037430071707dd01 be0400000600c000 0800c000240c0502 a404050200000100)
    @elx %XEB.Proto.Core.KeyReleaseEvent{
      detail: %XEB.Proto.Core.KEYCODE{val: 116},
      time: %XEB.Proto.Core.TIMESTAMP{val: ~h(1707dd01)i},
      root: %XEB.Proto.Core.WINDOW{xid: ~h(be040000)i},
      event: %XEB.Proto.Core.WINDOW{xid: ~h(0600c000)i},
      child: %XEB.Proto.Core.WINDOW{xid: ~h(0800c000)i},
      root_x: 3108, root_y: 517, event_x: 1188, event_y: 517,
      state: MapSet.new, same_screen: true, usersent: false
    }

    test "are correctly parsed" do
      assert XEB.Proto.Core.KeyReleaseEvent.from_X(@bin) == @elx
    end

  # test "are correctly generated" do
  #   Only the ClientMessage event is considered useful.
  # end
  end

  describe "Errors" do
    @bin ~h(0002d90200000000 00000c0000000000 0000000000000000 0000000000000000)
    @elx %XEB.Proto.Core.ValueError{
      bad_value: 0, minor_opcode: 0, major_opcode: 12
    }

    test "are correctly parsed" do
      assert XEB.Proto.Core.ValueError.from_X(@bin) == @elx
    end

  # test "are correctly generated" do
  #   Error generation is not considered useful
  # end
  end

  describe "Errors defined as errorcopy" do
    @bin ~h(0003940b03002001 0000020000000000 0000000000000000 0000000000000000)
    @elx %XEB.Proto.Core.WindowError{
      bad_value: ~h(03002001)i, minor_opcode: 0, major_opcode: 2
    }

    test "are correctly parsed" do
      assert XEB.Proto.Core.WindowError.from_X(@bin) == @elx
    end

  # test "are correctly generated" do
  #   Error generation is not considered useful
  # end
  end

  describe "Structured elements using switches" do
    test "are correctly generated" do
      bin = ~h(0c000700840aa000 0f000000e2090000 6101000018010000 2e000000)
      elx = %XEB.Proto.Core.ConfigureWindow{
        value_mask: [:X, :Y, :Width, :Height],
        x: 2530, y: 353, width: 280, height: 46,
        window: %XEB.Proto.Core.WINDOW{xid: ~h(840aa000)i},
      }

      assert XEB.Proto.Core.ConfigureWindow.to_X(elx) == bin
    end

  # test "are correctly parsed" do
  #   # No "useful to parse" element uses switches. TODO later if needed.
  # end
  end

  describe "Structured elements using lists" do
    test "are correctly generated" do
      bin = ~h(400006004801c000 0b00c00053015e02 54015e0253015f02)
      elx = %XEB.Proto.Core.PolyPoint{
        coordinate_mode: :Origin,
        drawable: %XEB.Proto.Core.DRAWABLE{xid: ~h(4801c000)i},
        gc: %XEB.Proto.Core.GCONTEXT{xid: ~h(0b00c000)i},
        points: [%XEB.Proto.Core.POINT{x: 339, y: 606},
                 %XEB.Proto.Core.POINT{x: 340, y: 606},
                 %XEB.Proto.Core.POINT{x: 339, y: 607}]
      }

      assert XEB.Proto.Core.PolyPoint.to_X(elx) == bin
    end

    test "are correctly parsed" do
      bin = ~h(0107710007000000 0000000000000000 0000000000000000 0000000000000000
               00000000ebff0000 00000000ebff0000 0000000000000000 00000000)
      elx = %XEB.Proto.Core.GetKeyboardMapping.Reply{
        keysyms_per_keycode: 7,
        keysyms: [%XEB.Proto.Core.KEYSYM{val: 0},
                  %XEB.Proto.Core.KEYSYM{val: ~h(ebff0000)i},
                  %XEB.Proto.Core.KEYSYM{val: 0},
                  %XEB.Proto.Core.KEYSYM{val: ~h(ebff0000)i},
                  %XEB.Proto.Core.KEYSYM{val: 0},
                  %XEB.Proto.Core.KEYSYM{val: 0},
                  %XEB.Proto.Core.KEYSYM{val: 0}]
      }

      assert XEB.Proto.Core.GetKeyboardMapping.Reply.from_X(bin) == elx
    end
  end

  describe "Structured elements with enum fields" do
    test "are correctly generated" do
      bin = ~h(4e0004000100c000 be0400001a030000)
      elx = %XEB.Proto.Core.CreateColormap{
        mid: %XEB.Proto.Core.COLORMAP{xid: ~h(0100c000)i},
        window: %XEB.Proto.Core.WINDOW{xid: ~h(be040000)i},
        visual: %XEB.Proto.Core.VISUALID{val: ~h(1a030000)i},
        alloc: :None
      }

      assert XEB.Proto.Core.CreateColormap.to_X(elx) == bin
    end

    test "are correctly parsed" do
      bin = ~h(fb02000004080001 0000ff0000ff0000 ff00000000000000)
      elx = %XEB.Proto.Core.VISUALTYPE{
        visual_id: %XEB.Proto.Core.VISUALID{val: ~h(fb020000)i},
        class: :TrueColor, bits_per_rgb_value: 8,
        colormap_entries: 256, red_mask: ~h(0000ff00)i,
        green_mask: ~h(00ff0000)i, blue_mask: ~h(ff000000)i
      }

      assert XEB.Proto.Core.VISUALTYPE.from_X(bin) == elx
    end
  end

  describe "Structured elements with altenum fields" do
    test "are correctly generated for enum value" do
      bin = ~h(1b00020000000000)
      elx = %XEB.Proto.Core.UngrabPointer{time: :CurrentTime}

      assert XEB.Proto.Core.UngrabPointer.to_X(elx) == bin
    end

    test "are correctly generated for non-enum value" do
      bin = ~h(1b0002005b76dd01)
      elx = %XEB.Proto.Core.UngrabPointer{
        time: %XEB.Proto.Core.TIMESTAMP{val: 31290971}
      }

      assert XEB.Proto.Core.UngrabPointer.to_X(elx) == bin
    end

    test "are correctly parsed for enum values" do
      bin = ~h(0100870000000000 2200000000000000 0000000000000000 0000000000000000)
      elx = %XEB.Proto.Core.InternAtom.Reply{atom: :WM_COMMAND}

      assert XEB.Proto.Core.InternAtom.Reply.from_X(bin) == elx
    end

    test "are correctly parsed for non-enum values" do
      bin = ~h(0100070000000000 3101000000000000 0000000000000000 0000000000000000)
      elx = %XEB.Proto.Core.InternAtom.Reply{
        atom: %XEB.Proto.Core.ATOM{xid: 305}
      }

      assert XEB.Proto.Core.InternAtom.Reply.from_X(bin) == elx
    end
  end

  test "GetKeyboardMapping length is rounded up" do
    bin = ~h(6500020008f80000)
    elx = %XEB.Proto.Core.GetKeyboardMapping{
      first_keycode: %XEB.Proto.Core.KEYCODE{val: 8},
      count: 248
    }

    assert XEB.Proto.Core.GetKeyboardMapping.to_X(elx) == bin
  end

  test "ModMasks are correctly encoded" do
    assert XEB.Proto.Core.ModMask.to_X([:Control, :Lock, :"2"]) == 22
  end
end


