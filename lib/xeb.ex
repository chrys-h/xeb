# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB do
  @moduledoc ~S"""
  Provides a binding for the X11 protocol. The module is divided into modules corresponding to the namespaces of XCB. XEB.Core is for the core protocol. The other modules each correspond to an extension. In these modules, each function corresponds to a request. The name of each function is the same as the name of the corresponding request, in snake_case (to follow the naming convention of the standard library) instead of the native CamelCase. The name of each module is the name of the corresponding extension in CamelCase (the "extension-name" attribute of root elements of the XML files).
  """

  require Logger
  require XEB.Gen
  use Application

  # OTP application definition
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(XEB.ResID, [XEB.ResID]),
      worker(XEB.Server, [XEB.Server]),
      worker(XEB.Mapping, [XEB.Mapping])
    ]

    Supervisor.start_link children, strategy: :one_for_one, name: XEB.Supervisor
  end

  XEB.Gen.generate()

  ## X11 binding functions definition
  #for {:xcb, mod_as, mod_cs} <- get_reqs() do
  #  #IO.puts "X11: processing #{inspect mod_as}"
  #  defmodule Macro.escape(String.to_atom("Elixir.XEB." <> mod_as[:modname])) do
  #    for {:request, %{name: name, fields: fields}, _} <- mod_cs do
  #      reqname = name |> Atom.to_string |> Macro.underscore |> String.to_atom
  #      el_modname = type_modname({mod_as[:modname], name}, __MODULE__)
  #      args = Enum.map fields, &(var(&1, :context))

  #      # This relies on the assumption that the fields variable is ordered
  #      # as it is in the XML files. It should be a safe assumption.
  #      def unquote(reqname)(unquote_splicing(args)) do
  #        klist = [unquote_splicing(Enum.zip(fields, args))]
  #        struc = struct(unquote(el_modname), klist)
  #        XEB.Server.request struc
  #      end
  #    end
  #  end
  #end
end

