# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen do
  @moduledoc false

  import XEB.Gen.Utils

  # This module ties together all of the code generation of the protocol.

  # Location of the XML-XCB files
  @external_resource "xcb-proto/src/*.xml"
  @xml_dir "xcb-proto/src/"

  # Unsupported extensions, referenced by their header name
  # If one of those extensions is required, compilation fails
  @unsupported [
    # XInput is excluded because it uses a switch in an element that
    # may be translated to X, which is currently not supported. It also
    # uses a paramref in an expression, which is not supported.
    :xinput,
    # RandR and Xkb are excluded because they both contain nontrivial
    # unions whose contents are not easy to decide. In order to add
    # support for RandR and Xkb, I need to figure out how these
    # unions work (for later reference: the offenders are RandR's
    # NotifyData and Xkb's Behavior, Action, and Doodad).
    :xkb,
    :randr,
    # GLX is excluded because it contains a valueparam, which is
    # deprecated. I reckon the valueparam could easily be turned into
    # a switch in XEB.Proto.Read.preprocess.
    :glx,
    # SHM and DRI3 are excluded because they use file descriptor (fd)
    # components.
    :shm,
    :dri3,
    # Present is excluded because it uses component required_start_align,
    # which is not standard and whose meaning I have yet to elucidate.
    :present,
    # XVideo is excluded because it imports Shm
    :xv,
    # XvMC is excluded because it imports XVideo
    :xvmc
  ]

  # This macro is the only part of the code generation engine that should
  # ever be called from the outside. It is normally only called once in
  # the XEB module.
  defmacro generate do
    desc = XEB.Gen.Read.get_raw_desc @xml_dir

    exts = XEB.Gen.Preprocess.get_exts desc
    deps = desc |> XEB.Gen.Preprocess.get_deps |> Map.new
    required = Application.get_env(:xeb, :extensions)
               |> process_requirements(exts)

    to_build = get_all_deps(required, deps)
    for e <- to_build,
      do: if e in @unsupported,
        do: raise "Extension #{inspect e} is required but not supported"

    desc_to_build = Enum.filter desc, fn
      {:xcb, %{header: h}, _} -> h in to_build
    end

    [XEB.Gen.IdentifyPackets.generate(desc_to_build),
     XEB.Gen.Proto.generate(desc_to_build),
     XEB.Gen.DirectFunctions.generate(desc_to_build)]
    |> flatten_quoted
  end

  #############################################################################
  # Requirement and dependency graph management

  defp process_requirements(nil, _) do
    [:xproto]
  end

  defp process_requirements(:all, exts) do
    exts
    |> Enum.map(&(&1.header))
    |> Enum.filter(&(not &1 in @unsupported))
  end

  defp process_requirements({:only, list}, exts) do
    process_requirements list, exts
  end

  defp process_requirements({:all_except, list}, exts) do
    list = Enum.map list, &ext_header(&1, exts)

    exts
    |> Enum.map(&(&1.header))
    |> Enum.filter(&(not &1 in list))
    |> Enum.filter(&(not &1 in @unsupported))
  end

  defp process_requirements(list, exts) when is_list(list) do
    Enum.map list, &ext_header(&1, exts)
  end

  defp process_requirements(_, _) do
    raise "Invalid extensions requirements"
  end

  defp ext_header(ext, exts) do
    exts
    |> Enum.filter(fn %{header: h, xname: x, name: n} ->
      ext == h or ext == x or ext == n
    end)
    |> case do
      [e] -> e.header
      [] -> raise "Unkown extension: #{inspect ext}"
    end
  end

  # This function takes in a list of the required extensions and a map
  # of each extension to its direct dependencies and returns a list of
  # all the extensions that need to be compiled.
  # HACK: This is not the best algorithm
  defp get_all_deps(reqs, deps_map) do
    deps = Enum.reduce reqs, [], fn (req, acc) ->
      Enum.uniq(acc ++ Map.get(deps_map, req, []))
    end

    if Enum.all?(deps, &(&1 in reqs)) do
      Enum.uniq(deps ++ reqs)
    else
      get_all_deps(Enum.uniq(deps ++ reqs), deps_map)
    end
  end
end

