# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Proto.Modnames do
  @moduledoc false

  # HACK HACK HACK: This entire module is a fucking mess
  def type_modname(name, mod \\ nil)

  # Get the name of translation module for the given type
  def type_modname(:CARD8, _), do: XEB.Proto.Types.CARD8
  def type_modname(:CARD16, _), do: XEB.Proto.Types.CARD16
  def type_modname(:CARD32, _), do: XEB.Proto.Types.CARD32
  def type_modname(:INT8, _), do: XEB.Proto.Types.INT8
  def type_modname(:INT16, _), do: XEB.Proto.Types.INT16
  def type_modname(:INT32, _), do: XEB.Proto.Types.INT32
  def type_modname(:BYTE, _), do: XEB.Proto.Types.BYTE
  def type_modname(:BOOL, _), do: XEB.Proto.Types.BOOL
  def type_modname(:char, _), do: XEB.Proto.Types.Char
  def type_modname(:void, _), do: XEB.Proto.Types.Void

  def type_modname({_, nil}, _) do
    raise "Cannot call type_modname with nil"
  end

  def type_modname(nil, _) do
    raise "Cannot call type_modname with nil"
  end

  def type_modname({ns, n}, _) do
    type_modname(n, ns)
  end

  def type_modname(n, mod) when is_binary(mod) and is_binary(n) do
    String.to_atom("Elixir.XEB.Proto." <> mod <> "." <> n)
  end

  def type_modname(n, mod) when is_atom(n) do
    type_modname(Atom.to_string(n), mod)
  end

  def type_modname(x, y) do
    raise "Invalid arguments for type_modname: #{inspect x}, #{inspect y}"
  end

  # HACK: DRY!
  def request_modname(x, y \\ nil), do: type_modname(x, y)

  def reply_modname({ns, n}) do
    {ns, n} |> request_modname |> reply_modname
  end

  def reply_modname(request) do
    String.to_atom(Atom.to_string(request) <> ".Reply")
  end

  def event_modname(n, ns), do: event_modname({ns, n})
  def event_modname({ns, n}), do: {ns, n} |> type_modname |> event_modname
  def event_modname(tmod) do
    str = Atom.to_string tmod
    new_str = if String.ends_with?(str, "Event") do
      str
    else
      str <> "Event"
    end
    String.to_atom(new_str)
  end

  def error_modname(n, ns), do: error_modname({ns, n})
  def error_modname({ns, n}), do: {ns, n} |> type_modname |> error_modname
  def error_modname(tmod) do
    str = Atom.to_string tmod
    new_str = if String.ends_with?(str, "Error") do
      str
    else
      str <> "Error"
    end
    String.to_atom(new_str)
  end
end

