# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
defmodule XEB.Proto.Types do
  @moduledoc ~S"""
  Provides translation functions for basic types. These functions are meant to be used in the generated code of XEB.Proto, as opposed to the functions from XEB.Proto.Helpers that are solely meant to be used during code generation.
  """

  # Basic types.
  defmodule CARD8 do
    def to_X(x), do: <<x::native-unsigned-8>>
    def raw_to_X(x), do: <<x::native-unsigned-8>>
    def do_from_X(<<x::native-unsigned-8>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule CARD16 do
    def to_X(x), do: <<x::native-unsigned-16>>
    def raw_to_X(x), do: <<x::native-unsigned-16>>
    def do_from_X(<<x::native-unsigned-16>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule CARD32 do
    def to_X(x), do: <<x::native-unsigned-32>>
    def raw_to_X(x), do: <<x::native-unsigned-32>>
    def do_from_X(<<x::native-unsigned-32>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule INT8 do
    def to_X(x), do: <<x::native-signed-8>>
    def raw_to_X(x), do: <<x::native-signed-8>>
    def do_from_X(<<x::native-signed-8>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule INT16 do
    def to_X(x), do: <<x::native-signed-16>>
    def raw_to_X(x), do: <<x::native-signed-16>>
    def do_from_X(<<x::native-signed-16>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule INT32 do
    def to_X(x), do: <<x::native-signed-32>>
    def raw_to_X(x), do: <<x::native-signed-32>>
    def do_from_X(<<x::native-signed-32>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule BYTE do
    def to_X(x), do: <<x::native-unsigned-8>>
    def raw_to_X(x), do: <<x::native-unsigned-8>>
    def do_from_X(<<x::native-unsigned-8>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule BOOL do
    def raw_to_X(x), do: <<x::native-unsigned-8>>
    def to_X(true), do: <<1::native-unsigned-8>>
    def to_X(false), do: <<0::native-unsigned-8>>
    def do_from_X(<<1::native-unsigned-8>> <> rest), do: {true, %{}, rest}
    def do_from_X(<<0::native-unsigned-8>> <> rest), do: {false, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule Char do
    def to_X(x), do: <<x::native-unsigned-8>>
    def raw_to_X(x), do: <<x::native-unsigned-8>>
    def do_from_X(<<x::native-unsigned-8>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  defmodule Void do
    def to_X(x), do: <<x::native-unsigned-8>>
    def raw_to_X(x), do: <<x::native-unsigned-8>>
    def do_from_X(<<x::native-unsigned-8>> <> rest), do: {x, %{}, rest}
    def from_X(data), do: data |> do_from_X |> elem(0)
  end

  # Lists
  defmodule List do
    def to_X(ls, gen) when is_list(ls) do
      Enum.reduce ls, <<>>, fn (l, acc) ->
        acc <> gen.(l)
      end
    end

    # This is a bit hackish but makes the direct passing of string or binary
    # data more efficient and convenient.
    def to_X(raw, _) when is_binary(raw) do
      raw
    end

    defp get_list(data, len, get, acc \\ [])

    defp get_list(data, len, _, acc) when data == "" or len == 0 do
      {Enum.reverse(acc), %{}, data}
    end

    defp get_list(data, len, get, acc) do
      {got_res, _, got_d} = get.(data)
      get_list(got_d, len - 1, get, [got_res | acc])
    end

    def do_from_X(data, :entire_input, get) do
      {got_res, _, _} = get_list(data, byte_size(data), get)
      {got_res, %{}, <<>>}
    end

    def do_from_X(data, len, get) do
      {got_res, _, got_d} = get_list(data, len, get)
      {got_res, %{}, got_d}
    end
  end
end


