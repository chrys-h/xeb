# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Server do
  @moduledoc ~S"""
  Provides OTP-style support for interaction with the X server.
  """

  import XEB.Identify
  import XEB.Proto.Modnames
  use Bitwise
  use GenServer
  require Logger

  # This is the initial state of the gen_server at initialization and a
  # template for the state variable.
  @init_state %{
    # The socket variable contains the socket that carries the connection
    # to the X server. The link is set up in the init function and needs
    # to be fully operational throughout the entire session.
    socket: nil,

    # The exts variable stores every extension that is in use in the
    # session. It contains three elements :
    #
    #  * :opcodes is a keyword map that stores the header name of each
    #    extension and their major opcodes. Elements may only be added
    #    as a result of a QueryExtension request. The core protocol is
    #    not represented in the list.
    #
    #  * :errors is a keyword map that stores the header name of each
    #    extension and their base error number (aka. 'first error').
    #    Elements may only be added as a result of a QueryExtension
    #    request. The core protocol is represented as {:xproto, 0}. The
    #    list must be kept sorted in ascending order of base error number.
    #    Extensions that do not add errors to the protocol (ie. the
    #    QueryExtension request returns a base error of 0) are omitted
    #    from the list.
    #
    #  * :events is a keyword map that stores the header name of each
    #    extension and their base event number (aka. 'first event').
    #    Elements may only be added as a result of a QueryExtension
    #    request. The core protocol is represented as {:xproto, 0}. The
    #    list must be kept sorted in ascending order of base event number.
    #    Extensions that do not add events to the protocol (ie. the
    #    QueryExtension request returns a base event of 0) are omitted
    #    from the list.
    #
    #  * :names is a map that matches each extension's header name to
    #    its real name, as used in modules names. The header name is an
    #    atom, and the real name is a string.
    exts: %{opcodes: [], errors: [xproto: 0], events: [xproto: 0],
            names: %{xproto: "Core"}},

    # The seq_num variable contains the sequence number of the latest
    # request sent to the X server. It is incremented each time a request
    # is sent to the server. If the number is larger than 16 bits, only
    # the least significant 16 bits are to be retained.
    seq_num: 0,

    # The max_req_length variable is the maximum request length accepted
    # by the server, as received from the server during connection setup.
    # It is stored as a number of bytes (as opposed to the units of 4
    # bytes used during connection setup).
    max_req_length: 0,

    # The motion_buffer_size is as received from the server during setup.
    motion_buffer_size: 0,

    # The pending variable is a map that matches the sequence numbers
    # of requests to tuples of the format
    #
    #    {<destination>, <parsing>} if the request expects a reply
    #     <destination>             otherwise
    #
    # where the <destination> is the PID of the process that needs to be
    # replied to and <parsing> is the name of the module that contains
    # the relevant from_X function (usually the name of the module for
    # the request with ".Reply" appended).
    # Elements are to be added to the map when a request is sent. If the
    # request expects a reply, the corresponding element is removed upon
    # receiving a reply or an error, which is then sent to the process
    # that sent the request (the <destination>). If the request doesn't
    # expect a reply, the <destionation> is retained until an error is
    # received (and then passed), or for 1,000 ms.
    pending: %{},

    # The keycode_range variable stores the min and max keycodes (as
    # given during connection setup) in a tuple of the form
    #
    #    {<min>, <max>}
    keycode_range: nil,

    # The screens variable stores the list of screens given in the Setup
    # data (as `roots` in XML-XCB). The list is stored 'as is', which is
    # to say as a list of XEB.Proto.Core.SCREEN.
    screens: [],

    # This holds the entire initialization data 'as is'
    setup: nil

    # TODO: at this point, the bitmap format variables are ignored during
    # connection setup because they are not useful for a lowly window
    # manager. They may, however, be needed at some point.
  }

  ###################################################################
  # Client API

  @doc ~S"""
  Starts the server process, initiates connection to the X server, and queries known extensions.
  """
  def start_link(name \\ __MODULE__) do
    GenServer.start_link(__MODULE__, %{}, [name: name])
  end

  @doc ~S"""
  Query the GenServer for a list of the extensions that are known to be supported during the session. The return value is a map containing three keys: :opcodes, :errors, and :events. Each corresponds to a keyword list that matches the 'header' of each extension to its corresponding major opcode, base error number, or base event number, respectively.
  """
  def get_exts(svr \\ __MODULE__) do
    GenServer.call(svr, :get_exts)
  end

  @doc ~S"""
  Query the GenServer for the range of keycodes received during connection setup. Returns the range in the form `{<first>, <last>}`.
  """
  def get_keycode_range(svr \\ __MODULE__) do
    GenServer.call(svr, :get_kc_range)
  end

  @doc ~S"""
  Sends a single request to the X server and returns the reply if the request warrants one, `:ok` otherwise. The function is synchronous, i.e. the packet is sent as soon as the function is called, and the function doesn't until the request has been received.
  The expected input is a struct in the form of `%XEB.Extension.Request{contents}`. It will be transparently encoded and the reply, if there is one, will be transparently decoded to a structure of the form `%XEB.Extension.Request.Reply{contents}`. If the input is not of that form, the function will raise a RuntimeError
  """
  def request(svr \\ __MODULE__, %structname{} = req) do
    Logger.debug "Preparing request: #{inspect req}"
    # The extensions list is queried from the GenServer, then the request
    # is encoded in the client side process, then sent to the GenServer.
    # There are two reasons for this back-and-forth:
    #  - The encoding function (XEB.Proto.Ext.Element.to_X) may raise an
    #    exception if the extension the request is from is not in the exts
    #    variable, and we prefer to raise exceptions in the client process.
    #  - The encoding involves some computations that do not absolutely need
    #    to take place in the GenServer process, so we prefer to do it in
    #    the client process and not needlessly hang the server.
    exts = get_exts()
    raw_req = case structname.to_X(req, exts) do
      :unknown_ext ->
        raise "Request encoding error: unknown extension"

      res ->
        res
    end

    GenServer.call(svr, {:request, raw_req, structname})
  end

  #@doc ~S"""
  #Sends multiple requests to the X server in a single packet and returns the list of their replies (if any) in the same order. The function is sychronous: the packet is sent directly when the function is called, and the function doesn't return until all replies (if any) have been received.
  #The expected input is a list of requests in the same format as `request` does. From an outside perspective, this function behaves like `&(Enum.map(&request/1, &1))` would if everything goes right.
  #If one request is found to be invalid, the function will return :invalid_request without sending anything to the server.
  #"""
  #def request_batch(svr \\ __MODULE__, _reqs) do
  #end

  @doc ~S"""
  Gets the list of screens as it was received in the setup data.
  """
  def get_screens(svr \\ __MODULE__) do
    GenServer.call(svr, :get_screens)
  end

  @doc ~S"""
  Gracefully shuts down XEB.Server.
  """
  def stop(_svr \\ __MODULE__) do
    # TODO
  end

  ###################################################################
  # Server callbacks

  # HACK: This function is getting awfully large.
  # HACK: Also, it should return {:stop, ...} instead of raising exceptions.
  def init(%{}) do
    # Get socket to the X server
    display = case :os.getenv("DISPLAY") do
      false -> raise "Init error: $DISPLAY not defined; are you running an X server?"
      conts -> conts |> to_string |> parse_display
    end

    # TODO: Support families other than local -- HACK: Nested cases
    socket = case display do
      {:local, _, num} ->
        spath = to_charlist("/tmp/.X11-unix/X" <> num)
        case :gen_tcp.connect({:local, spath}, 0, [:binary, {:active, true}]) do
          {:error, reason} -> raise "Init error: failed to connect socket: #{inspect reason}"
          {:ok, s} -> s
        end

      {family, _, _} ->
        raise "Init error: unsupported family #{inspect family}"
    end

    # Get system byte order. It seems there is no default function to test
    # the endianness of the system, so it needs to be done 'the dirty way'
    endianness = if <<1::native-32>> == <<1::big-32>>
      do   66  # = octal 102, meaning big endian
      else 108 # = octal 154, meaning little endian
    end

    # Get authentification details
    {ap_name, ap_data} = get_auth(display)

    # Setup connection
    raw_setup_request = %XEB.Proto.Core.SetupRequest{
      byte_order:                      endianness,
      protocol_major_version:          11,
      protocol_minor_version:          0,
      authorization_protocol_name_len: byte_size(ap_name),
      authorization_protocol_data_len: byte_size(ap_data),
      authorization_protocol_name:     ap_name,
      authorization_protocol_data:     ap_data
    } |> XEB.Proto.Core.SetupRequest.to_X
    :gen_tcp.send(socket, raw_setup_request)

    Logger.debug "Sent initial connection request; awaiting reply..."

    # Apparently, the initial connection reply can be received in fragments.
    # That's rather inconvenient because it means dodgy manipulations are
    # required to receive it entirely.
    response = recv_contiguous socket

    Logger.debug "Received #{inspect byte_size(response)} bytes. Continuing initialization."

    setup_data = case response do
      # In case of setup failure
      <<0::native-8>> <> _ ->
        %XEB.Proto.Core.SetupFailed{reason: r} = XEB.Proto.Core.SetupFailed.from_X response
        raise "Init error: connection setup failed: #{inspect r}"

      # In case further authorization is needed.
      # In that case we don't know what to do so we fail.
      <<2::native-8>> <> _ ->
        %XEB.Proto.Core.SetupAuthenticate{reason: r} = XEB.Proto.Core.SetupAuthenticate.from_X response
        raise "Init error: connection setup requires authentication: #{inspect r}"

      # In case the connection setup succeeded
      <<1::native-8>> <> _ ->
        XEB.Proto.Core.Setup.from_X response

      # In case something weird happens
      _ ->
        raise "Init error: something weird happened"
    end

    # Set up the XID allocation range
    XEB.ResID.set_range(setup_data.resource_id_mask, setup_data.resource_id_base)

    # Incorporate the setup data into the state variable
    post_setup_state = %{@init_state |
      socket: socket,
      max_req_length: setup_data.maximum_request_length,
      motion_buffer_size: setup_data.motion_buffer_size,
      keycode_range: {setup_data.min_keycode, setup_data.max_keycode},
      screens: setup_data.roots,
      setup: setup_data
    }

    # TODO: Query extensions

    Logger.debug "XEB.Server initialization complete"
    final_state = post_setup_state
    {:ok, final_state}
  end

  def handle_call(:get_exts, _, %{exts: e} = state) do
    {:reply, e, state}
  end

  def handle_call(:get_screens, _, %{screens: s} = state) do
    {:reply, s, state}
  end

  def handle_call(:get_kc_range, _, %{keycode_range: r} = state) do
    {:reply, r, state}
  end

  def handle_call({:request, conts, modname}, {pid, _} = from, %{socket: s, seq_num: sn, pending: p} = state) do
    new_sn = increment_seq_num sn
    Logger.debug "Sending request #{inspect new_sn}: #{inspect modname}, value #{inspect conts}"
    :gen_tcp.send(s, conts)

    if modname.takes_reply?() do
      new_p = Map.put(p, new_sn, {from, reply_modname(modname)})
      new_state = %{state | seq_num: new_sn, pending: new_p}
      {:noreply, new_state}
    else
      new_p = Map.put(p, new_sn, pid)
      new_state = %{state | seq_num: new_sn, pending: new_p}

      me = self()
      spawn fn ->
        :timer.sleep 1_000
        send me, {:error_timeout, new_sn}
      end

      {:reply, :ok, new_state}
    end
  end

  def handle_info({:tcp, s, data}, %{socket: s, pending: p, exts: e} = state) do
    Logger.debug "Received #{inspect byte_size(data)} bytes"
    case identify(data, e) do
      {:event, id} ->
        {:noreply, handle_X_event(id, data, state)}

      {:error, id} ->
        {:noreply, handle_X_error(id, data, state)}

      {:reply, %{seq_num: sn, len: l}} ->
        # Reply from the X server to a request
        # Data are only received by sequences of at most 1460 bytes, so a
        # reply longer than that needs to be received in parts.
        # We compare it to 1460/4 = 365 because it is in units of 4 bytes.
        complete_data = if l + 8 > 365 do
          data <> recv_contiguous(s, l * 4 + 32 - 1460)
        else data end
        case Map.get(p, sn) do
          nil ->
            {:noreply, state}

          {client, mod} ->
            GenServer.reply(client, mod.from_X(complete_data))
            {:noreply, %{state | pending: Map.delete(p, sn)}}
        end

      ls when is_list(ls) ->
          data
          |> splice_32bytes
          |> Enum.zip(ls)
          |> Enum.reduce(state, fn ({d, {t, id}}, st) -> dispatch_X_packet(t, id, d, st) end)
        {:noreply, state}

      :bad_data ->
        <<h::binary-16>> <> _ = data
        Logger.warn "Received #{inspect byte_size(data)} bytes of unrecognized data, beginning #{inspect h}"
        {:noreply, state}
    end
  end

  def handle_info({:error_timeout, sn}, %{pending: p} = state) do
    new_p = Map.delete(p, sn)
    new_state = %{state | pending: new_p}
    {:noreply, new_state}
  end

  ###################################################################
  # Backend functions

  # Increase the seq_num variable and keep it less than 16 bits long
  defp increment_seq_num(sn), do: (sn + 1) &&& 0xFFFF

  # Parse the contents of the DISPLAY environment variable to a tuple:
  #
  #    {<family>, <address>, <number>}
  defp parse_display(raw) do
    regex = ~r/^([[:print:]]*):([[:digit:]]+)\.?([[:digit:]]*)$/
    case Regex.run(regex, raw) do
      [^raw, "", num, _] ->
        {:ok, hname} = :inet.gethostname
        {:local, to_string(hname), num}

      _ ->
        raise "$DISPLAY error: families other than local are not supported yet."
    end
  end

  # Read Xauthority and return the authorization details as a tuple like:
  #
  #    {<auth_protocol_name>, <auth_protocol_data>}
  #
  # Both are lists of 1-byte integers for the reasons explained below.
  # The argument is expected to be the output of parse_display.
  defp get_auth({family, addr, num}) do
    match = Enum.find get_xauthority(), fn {el_family, el_addr, el_num, _, _} ->
      el_family == family and el_addr == addr and el_num == num
    end

    case match do
      {_, _, _, proto, data} -> {proto, data}
      nil -> raise "Auth error: no matching entry in Xauthority"
    end
  end

  # Read the Xauthority file and parse it to a list of tuples of the form:
  #
  #    [{<family>, <hostname>, <display>, <auth protocol>, <cookie>}]
  defp get_xauthority do
    xauth_path = case :os.getenv("XAUTHORITY") do
      false -> raise "Xauthority error: $XAUTHORITY not defined"
      path -> path
    end

    xauth_raw = case File.read xauth_path do
      {:error, reason} -> raise "Xauthority error: could not read XAUTH file: #{inspect reason}"
      {:ok, contents} -> contents
    end

    parse_xauthority xauth_raw
  end

  defp parse_xauthority("") do
    []
  end

  defp parse_xauthority(data) do
    # The format of an entry is:
    #    big-16    family
    #    big-16    address_length
    #    [byte]    address           (length = address_length)
    #    big-16    number_length
    #    [byte]    number            (length = number_length)
    #    big-16    name_length
    #    [byte]    name              (length = name_length)
    #    big-16    data_length
    #    [byte]    data              (length = data_length)
    # I leave the link here because this info is suprisingly hard to find:
    # See https://github.com/python-xlib/python-xlib/blob/master/Xlib/xauth.py
    <<raw_fam::big-16>> <> data = data
    case raw_fam do
      256 ->
        <<addr_len      ::big-16,
          addr          ::binary-size(addr_len),
          num_len       ::big-16,
          num           ::binary-size(num_len),
          name_len      ::big-16,
          name          ::binary-size(name_len),
          auth_data_len ::big-16,
          auth_data     ::binary-size(auth_data_len)>> <> data = data
        [{:local, addr, num, name, auth_data} | parse_xauthority(data)]

      _ ->
        Logger.warn "Xauthority: unrecognized family identifier #{inspect raw_fam} ; ignoring entry"
        <<addr_len      ::big-16, _ ::binary-size(addr_len),
          num_len       ::big-16, _ ::binary-size(num_len),
          name_len      ::big-16, _ ::binary-size(name_len),
          auth_data_len ::big-16, _ ::binary-size(auth_data_len)>> <> data = data
        parse_xauthority(data)
    end
  end

  defp dispatch_X_packet(:error, id, data, state), do: handle_X_error(id, data, state)
  defp dispatch_X_packet(:event, id, data, state), do: handle_X_event(id, data, state)

  defp handle_X_event(id, data, state) do
    evt = event_modname(id[:name], id[:ext]).from_X(data)
    Logger.debug "Received event #{inspect id[:seq_num]}: #{inspect evt}"
    handle_event evt, id[:seq_num], state
  end

  defp handle_event(%XEB.Proto.Core.MappingNotifyEvent{request: m}, _sn, state) do
    # Mapping notifications are a special kind of events ; when it is
    # received, the mappings retained by XEB.Mapping should be updated.
    XEB.Mapping.update_mapping m
    state
  end

  defp handle_event(evt, _sn, state) do
    case Map.get(evt, :parent) || Map.get(evt, :event) || Map.get(evt, :window) do
      nil ->
        Logger.info "Received event with no 'event' or 'window' field. Ignoring."
        nil

      res ->
        dests = XEB.ResID.who_has res.xid
        Logger.debug "Event destinations: #{inspect dests}"
        Enum.map dests, &(send &1, {:X_event, res, evt})
    end
    state
  end

  defp handle_X_error(%{seq_num: sn} = id, data, state) do
    err = error_modname(id[:name], id[:ext]).from_X(data)
    handle_error err, sn, state
  end

  defp handle_error(err, sn, %{pending: p} = state) do
    case Map.fetch(p, sn) do
      {:ok, {pid, _}} when is_pid(pid) ->
        Logger.warn "Received error #{inspect sn}: #{inspect err} ; passing to responsible process"
        send pid, {:X_error, err}

      {:ok, pid} when is_pid(pid) ->
        Logger.warn "Received error #{inspect sn}: #{inspect err} ; passing to responsible process"
        send pid, {:X_error, err}

      :error ->
        Logger.warn "Received error #{inspect sn}: #{inspect err} ; no known responsible process ; ignoring."
    end

    new_p = Map.delete p, sn
    %{state | pending: new_p}
  end

  # Receive a fragmented datagram as a single packet. Useful for the setup
  # reply, which is large and usually arrives fragmented.
  defp recv_contiguous(socket, len \\ :infinity)
  defp recv_contiguous(_, 0), do: <<>>
  defp recv_contiguous(socket, len) do
    receive do
      {:tcp, ^socket, data} ->
        data <> recv_contiguous(socket, if is_atom(len) do len else len - byte_size(data) end)
    after
      10 -> ""
    end
  end
end

