# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Identify do
  @moduledoc ~S"""
  Provides functions to identify the kind and meaning of a packet freshly received from the X server in order to guide further interpretation.
  """

  require XEB
  import XEB.IdentifyPackets
  use Bitwise

  # Actual identification function
  def identify(data, exts \\ %{errors: [xproto: 0],
                               events: [xproto: 0],
                               opcodes: [],
                               names: %{xproto: "Core"}})

  # Reply identification
  def identify(<<h::native-8, _, sn::native-16, ln::native-32>> <> _, _) when h == 1 do
    {:reply, %{seq_num: sn, len: ln}}
  end

  # Error identification
  def identify(<<h::native-8,
                 n::native-8,
                 s::native-16,
                 _::native-32,
                 mi::native-16,
                 ma::native-8>> <> data, exts)
               when h == 0 and byte_size(data) == 21 do
    # Find the extension whose base error makes the error number closest to 0
    {ext, fst_err} = Enum.min_by exts[:errors], fn {_, base_err} -> abs(n - base_err) end

    {^ext, errname} = identify_error(ext, n - fst_err)
    extname = Map.fetch!(exts[:names], ext)
    {:error, %{ext: extname, name: errname, seq_num: s, min_opcode: mi, maj_opcode: ma}}
  end

  # Event identification
  def identify(<<n::native-8,
                 _::native-8,
                 s::native-16>> <> data, exts)
               when byte_size(data) == 28 do
    # Ignore whether the event was sent or not by clearing the most significant bit
    n = n &&& 127

    # Find the extension whose base event number makes the event number closest to 0
    {ext, fst_evt} = Enum.min_by exts[:events], fn {_, base_evt} -> abs(n - base_evt) end

    {^ext, evtname} = identify_event(ext, n - fst_evt)
    extname = Map.fetch!(exts[:names], ext)
    {:event, %{ext: extname, name: evtname, seq_num: s}}
  end

  # Multiple identification
  def identify(data, exts) when rem(byte_size(data), 32) == 0 do
    data
    |> splice_32bytes
    |> Enum.map(&identify(&1, exts))
  end

  # Fail
  def identify(_, _) do
    :bad_data
  end

  # TODO: Move this function somewhere it belongs
  def splice_32bytes(""), do: []
  def splice_32bytes(<<b::binary-32>> <> rest), do: [b | splice_32bytes rest]
end

