# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Keysyms do
  @moduledoc ~S"""
  Provides a lookup table to map key names (e.g. `:Space`, `:q`, etc.) to their respective keysyms. This is achieved through a lookup function. Note that the function only goes one way because certain names may map to the same keysym. If an atom doesn't name a keysym, the function returns `nil`.
  The code in this module is generated dynamically from the contents of the keysyms/ directory.
  """

  require XEB

  symbols = ["common.ks" , "latin1.ks"]
  |> Enum.map(&("keysyms/" <> &1))
  |> Enum.map(&File.read!/1)
  |> Enum.map(&String.split(&1, "\n", trim: true))
  |> Enum.concat
  |> Enum.reject(&(&1 == ""))
  |> Enum.reject(&(String.first(&1) == "#"))
  |> Enum.map(fn str ->
    regex = ~r/^0[xX]([0-9a-fA-F]+)\s+([^#]+)\s?(?:#.*)?$/
    case Regex.run(regex, str) do
      [^str, raw_num, raw_name] ->
        {String.to_integer(raw_num, 16),
         raw_name |> String.trim |> String.to_atom}

      _ ->
        raise "Could not parse keysym specification #{inspect str}"
    end
  end)

  for {num, name} <- symbols do
    def lookup_name(unquote(name)) do
      %XEB.Proto.Core.KEYSYM{val: unquote(num)}
    end
  end

  def lookup_name(_), do: nil
end

