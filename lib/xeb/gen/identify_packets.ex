# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.IdentifyPackets do
  @moduledoc false

  import XEB.Gen.Utils

  # This module contains the functions that generate the code of the
  # XEB.Identify.Packets module. The XEB.Identify.Packets module contains
  # all packet-specific identification code and is generated here based
  # on the protocol description.
  
  def generate(desc) do
    mod_contents = desc
    |> flatten_desc
    |> Enum.map(&gen_fn/1)
    |> Enum.filter(&(&1))
    |> flatten_quoted

    quote do
      defmodule :"Elixir.XEB.IdentifyPackets" do
        @moduledoc false

        unquote(mod_contents)
      end
    end
  end

  def flatten_desc(desc) do
    desc
    |> Enum.map(fn {:xcb, mod_as, mod_cs} ->
      mod_as[:header]
      |> List.wrap
      |> Stream.cycle
      |> Enum.zip(mod_cs)
    end)
    |> List.flatten
  end

  def gen_fn({ext, {unit, %{number: errno, name: name}, _}})
             when unit == :error or unit == :errorcopy do
    errname = String.to_atom(Atom.to_string(name) <> "Error")
    quote do
      def identify_error(unquote(ext), unquote(errno)),
        do: {unquote(ext), unquote(errname)}
    end
  end

  def gen_fn({ext, {unit, %{number: evtno, name: name}, _}})
             when unit == :event or unit == :eventcopy do
    evtname = String.to_atom(Atom.to_string(name) <> "Event")
    quote do
      def identify_event(unquote(ext), unquote(evtno)),
        do: {unquote(ext), unquote(evtname)}
    end
  end

  def gen_fn(_) do
    nil
  end
end

