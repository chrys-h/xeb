# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Read do
  @moduledoc false
  # This module reads the XML files of XML-XCB and process them into a form
  # suitable for protcol code generation

  #############################################################################
  # Description obtension

  # Paper-thin wrapper around erlsom's simple_form
  defp parse_xml_file(filename) do
    {:ok, file}    = File.read(filename)
    {:ok, data, _} = :erlsom.simple_form(file)

    data
  end

  # Turn the string-based simple_form into more idiomatic data
  defp erlangify({n, as, cs}) do
    name = charlist_to_atom n
    attrs = Enum.reduce as, %{}, fn
      ({nam, val}, acc) -> Map.put(acc, charlist_to_atom(nam), erlangify(val))
    end

    conts = if n == 'doc'
      do   cs
      else Enum.map(cs, &erlangify/1)
    end

    {name, attrs, conts}
  end

  defp erlangify(s) when is_list(s) do
    s |> to_string |> erlangify
  end

  defp erlangify(s) when is_bitstring(s) do
    case Integer.parse(s) do
      {n, ""} -> n
      _ -> String.to_atom(s)
    end
  end

  defp charlist_to_atom(c) do
    c |> to_string |> String.to_atom
  end

  # This function is a pass that does certain modifications that need to be
  # performed on a single element.
  defp special_cases({:enum, %{name: :ModMask}, cs}) do
    # The ModMask enum contains values that are made up of a single number,
    # which causes erlangify/1 to parse them as number rather than atoms, which
    # causes bugs. In this function, we turn them back to atoms.
    # NB: Maybe the same case will occur elsewhere.
    new_cs = Enum.map cs, fn
      {n, %{name: ename} = as, cs} when is_number(ename) ->
        new_ename = ename |> inspect |> String.to_atom
        {n, %{as | name: new_ename}, cs}

      x -> x
    end

    {:enum, %{name: :ModMask}, new_cs}
  end

  defp special_cases(x) do
    x
  end

  # This function reads XML files into an 'erlangified' format: identifiers
  # are turned into atoms, numeric literals are turned into numbers and
  # documentation strings are left untouched.
  defp read_XML(files) do
    files
    |> Enum.map(&parse_xml_file/1)
    |> Enum.map(&erlangify/1)
    |> Enum.map(fn {n, as, cs} -> {n, as, Enum.map(cs, &special_cases/1)} end)
  end

  defp xcb_desc(xml_dir) do
    xml_dir
    |> File.ls!
    |> Enum.filter(&(String.ends_with?(&1, ".xml")))
    |> Enum.map(&(xml_dir <> &1))
    |> read_XML
  end

  # Return the raw description of the protocol
  def get_raw_desc(xml_dir) do
    xcb_desc(xml_dir)
  end
end


