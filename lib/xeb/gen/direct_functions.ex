# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.DirectFunctions do
  @moduledoc false

  import XEB.Gen.Utils

  # I call direct functions the functions of the form
  #
  #   XEB.Extension.request_name(...)
  #
  # These functions can be called directly from anywhere and will issue the
  # desired request with the desired arguments provided that:
  #   - The process named XEB.Server is an XEB.Server (which is the case
  #     by default) and connected to an X11 server
  #   - The request and extension were built into XEB (see the 'extensions'
  #     config variable)
  #   - There is nothing wrong with the arguments.
  #
  # The argument scheme of direct functions is based on passing a keyword list
  # as an argument. Each argument is an entry in the keyword list, the matching
  # keyword being the field name as an atom as specified in the XML files.
  # Moreover:
  #
  #   - If the field is part of a "switch" expression, it may be skipped, in
  #     which case the field will be left out of the switch. Otherwise, it will
  #     be added.
  #   - If the field is a 'value-mask' corresponding to a switch (and ONLY if it
  #     corresponds to a switch), it will be ignored and its correct value will
  #     be calculated based on which fields of the switch are included and which
  #     are left out.
  #   - Otherwise, the field MUST be specified.
  #
  # The order in which the fields are specified doesn't matter.

  def generate(desc) do
    # The description is assumed to be the list of extensions to be compiled
    # as returned by XEB.Gen.Read.get_raw_desc.
    desc
    |> XEB.Gen.Preprocess.get_reqs
    |> Enum.map(&generate_ext/1)
    |> flatten_quoted
  end

  defp generate_ext({:xcb, %{modname: modname}, cs}) do
    module_name = String.to_atom("Elixir.XEB." <> modname)

    contents = cs
    |> Enum.map(&generate_fun(&1, modname))
    |> flatten_quoted

    quote do
      defmodule unquote(module_name) do
        @moduledoc false
        unquote(contents)
      end
    end
  end

  defp generate_fun({:request, %{name: rname}, cs}, modname) do
    fun_name = rname |> Atom.to_string |> Macro.underscore |> String.to_atom
    struct_name = XEB.Proto.Modnames.request_modname(rname, modname)

    case Enum.find(cs, &(elem(&1, 0) == :switch)) do
      nil -> generate_function_without_switch(struct_name, fun_name)
      sw -> generate_function_with_switch(cs, sw, struct_name, fun_name)
    end
  end

  defp generate_function_with_switch(_, {:switch, _, [{:fieldref, %{}, [fieldref]} | cases]},
                                     struct_name, fun_name) do
    switch_fields = get_switch_fields cases

    quote do
      def unquote(fun_name)(args \\ []) do
        {defined, _} = Enum.unzip args
        mask = Enum.reduce unquote(Macro.escape switch_fields), [], fn
          ({:bitcase, ref, fields}, ls) when is_list(ls) ->
            if Enum.all?(fields, &(&1 in defined)) do
              [ref | ls]
            else
              ls
            end

          ({:case, ref, fields}, []) ->
            if Enum.all?(fields, &(&1 in defined)) do
              ref
            else
              []
            end

          (_, _) -> raise "Switch error"
        end
        packet = struct(unquote(struct_name), [{unquote(fieldref), mask} | args])
        :"Elixir.XEB.Server".request packet
      end
    end
  end

  defp generate_function_without_switch(struct_name, fun_name) do
    quote do
      def unquote(fun_name)(args \\ []) do
        packet = struct(unquote(struct_name), args)
        :"Elixir.XEB.Server".request packet
      end
    end
  end

  defp get_fields(cs) do
    cs |> Enum.filter(fn
      {:switch, _, _} -> false
      {_, %{name: _}, _} -> true
      _ -> false
    end)
    |> Enum.map(fn {_, %{name: n}, _} -> n end)
  end

  defp get_switch_fields(cases) do
    cases
    |> Enum.filter(fn
      {:bitcase, _, _} -> true
      {:case, _, _} -> true
      _ -> false
    end)
    |> Enum.map(fn
      {:bitcase, %{}, [{:enumref, _, [ref_bit]} | fields]} ->
        {:bitcase, ref_bit, get_fields(fields)}
      {:case, %{}, [{:enumref, _, [ref_item]} | fields]} ->
        {:case, ref_item, get_fields(fields)}
    end)
  end
end

