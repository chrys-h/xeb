# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Proto.Unit do
  @moduledoc false

  import XEB.Gen.Proto.Fields
  import XEB.Gen.Utils
  import XEB.Proto.Modnames
  alias XEB.Gen.Proto.Enums, as: En

  #############################################################################
  # Main generation function

  def generate({:struct, %{name: name, fields: fields}, cs}, mod_as) do
    module_name = type_modname({mod_as[:modname], name})
    conts = trim_contents cs

    quote do
      defmodule unquote(module_name) do
        defstruct unquote(Macro.escape(fields))

        unquote(generate_unit_functions(conts, mod_as[:modname], module_name))

        def raw_to_X(_),
          do: raise "Struct size cannot be statically calculated"
      end
    end
  end

  def generate({:union, as, _cs}, mod_as) do
    module_name = type_modname({mod_as[:modname], as[:name]})

    quote do
      defmodule unquote(module_name) do
        # The only union currently supported is ClientMessageData (#TODO) and
        # it's simpler to simply treat it as a container for a binary blob.
        defstruct [:val]

        def to_X(%unquote(module_name){val: val}) when is_binary(val), do: val
        def do_from_X(data) when is_binary(data), do: {%unquote(module_name){val: data}, %{}, ""}
        def from_X(data), do: data |> do_from_X |> elem(0)
      end
    end
  end

  def generate({:xidtype, as, _cs}, mod_as) do
    module_name = type_modname({mod_as[:modname], as[:name]})

    quote do
      defmodule unquote(module_name) do
        defstruct [:xid]

        def raw_to_X(i), do: <<i::native-32>>
        def to_X(%unquote(module_name){xid: id}), do: <<id::native-32>>
        def do_from_X(<<id::native-32>> <> rest), do: {%unquote(module_name){xid: id}, %{}, rest}
        def from_X(data), do: data |> do_from_X |> elem(0)
      end
    end
  end

  def generate({:xidunion, as, cs}, mod_as) do
    typemods = Enum.map cs, fn {:type, %{}, [t]} -> type_modname(t, mod_as[:modname]) end
    module_name = type_modname({mod_as[:modname], as[:name]})

    quote do
      defmodule unquote(module_name) do
        defstruct [:xid]

        def raw_to_X(i), do: <<i::native-32>>
        def do_from_X(<<id::native-32>> <> rest), do: {%unquote(module_name){xid: id}, %{}, rest}
        def from_X(data), do: data |> do_from_X |> elem(0)

        unquote(for mname <- [module_name | typemods] do
          quote do
            def to_X(%unquote(mname){xid: id}), do: <<id::native-32>>
          end
        end)
      end
    end
  end

  def generate({:enum, as, cs}, mod_as) do
    module_name = type_modname({mod_as[:modname], as[:name]})
    raw_values = Enum.filter_map cs, &En.item_value?/1, &En.item_value/1
    values = Enum.reduce raw_values, [], &En.merge_duplicate_items/2
    bits = Enum.filter_map cs, &En.item_bit?/1, &En.item_bit/1
    bit_items = Enum.map bits, &elem(&1, 0)

    enum_decode_fun = values
    |> Enum.map(fn {name, val} ->
      quote do: def from_X(unquote(val)), do: unquote(name)
    end)
    |> flatten_quoted

    enum_encode_fun = raw_values
    |> Enum.map(fn {name, val} ->
      quote do: def to_X(unquote(name)), do: unquote(val)
    end)
    |> flatten_quoted

    catchall_funs = quote do
      def to_X(_), do: nil
      def from_X(_), do: nil
    end

    mask_funs = quote do
      def to_X(es) when is_list(es) do
        es |> MapSet.new |> to_X
      end

      def to_X(%MapSet{} = es) do
        if Enum.all?(es, &(Enum.member?(unquote(bit_items), &1))) do
          Enum.reduce unquote(bits), 0, fn({item, bit}, acc) ->
            if MapSet.member?(es, item)
              do acc ||| (1 <<< bit)
              else acc
            end
          end
        else
          nil
        end
      end

      def from_X(i) do
        if is_integer(i) && i <= unquote(round(:math.pow(2, Enum.count(bits))) - 1) do
          Enum.reduce unquote(bits), MapSet.new, fn({item, bit}, acc) ->
            if (i &&& (1 <<< bit)) != 0
              do MapSet.put acc, item
              else acc
            end
          end
        else
          nil
        end
      end
    end

    tail = if Enum.empty?(bits) do
      catchall_funs
    else
      mask_funs
    end

    contents = flatten_quoted [enum_decode_fun, enum_encode_fun, tail]

    quote do
      defmodule unquote(module_name) do
        use Bitwise
        unquote(contents)
      end
    end
  end

  def generate({:typedef, as, _cs}, mod_as) do
    conts = [{:field, %{type: as[:oldname], name: :val}, []}]
    module_name = type_modname({mod_as[:modname], as[:newname]})
    old_modname = type_modname(as[:oldname], mod_as[:modname])

    quote do
      defmodule unquote(module_name) do
        defstruct [:val]

        unquote(generate_unit_functions(conts, mod_as[:modname], module_name))

        def raw_to_X(i),
          do: unquote(old_modname).raw_to_X(i)
      end
    end
  end

  def generate({:request, as, cs}, mod_as) do
    req_cs = trim_contents cs
    sname = Atom.to_string(as[:name])
    req_modname = request_modname(sname, mod_as[:modname])
    req_conts = if mod_as[:header] != :xproto do
      [{:ext_opcode, %{bytes: 1, ext: mod_as[:header]}, []},
       {:lit, %{bytes: 1, val: as[:opcode]}, []},
       {:length_field, %{bytes: 2, base: 0}, []}]
      ++ req_cs
    else
      if req_cs == [] do
        [{:lit, %{bytes: 1, val: as[:opcode]}, []},
         {:pad, %{bytes: 1}, []},
         {:length_field, %{bytes: 2, base: 0}, []}]
      else
        [{:lit, %{bytes: 1, val: as[:opcode]}, []},
         hd(req_cs),
         {:length_field, %{bytes: 2, base: 0}, []}]
        ++ tl(req_cs)
      end
    end

    rep = Enum.find cs, fn
      {:reply, _, _} -> true
      _ -> false
    end
    takes_reply = if rep == nil do false else true end

    # Define the actual module
    req_code = quote do
      defmodule unquote(req_modname) do
        defstruct unquote(Macro.escape(as[:fields]))

        # Decoding functions are not generated for requests
        unquote(generate_encoding_fun(req_conts, mod_as[:modname], req_modname))

        def takes_reply?, do: unquote(takes_reply)
      end
    end


    if rep do
      {:reply, rep_as, rep_cs} = rep
      rep_modname = reply_modname(req_modname)
      [rep_cs_hd | rep_cs_tl] = trim_contents rep_cs
      rep_conts = [
        {:lit, %{bytes: 1, val: 1}, []},
        rep_cs_hd,
        {:pad, %{bytes: 2}, []}, # Ignore sequence number.
        {:length_field, %{bytes: 4, base: 32}, []}]
       ++ rep_cs_tl

      rep_code = quote do
        defmodule unquote(rep_modname) do
          defstruct unquote(Macro.escape(rep_as[:fields]))

          # Encoding functions are not generated for replies
          unquote(generate_decoding_fun(rep_conts, mod_as[:modname], rep_modname))
        end
      end

      append_quoted req_code, rep_code
    else
      req_code
    end
  end

  def generate({:event, as, cs}, mod_as) do
    module_name = event_modname({mod_as[:modname], as[:name]})
    trimmed = trim_contents cs
    conts = if mod_as[:header] != :xproto do
      [{:ext_eventcode, %{bytes: 1, ext: mod_as[:header], evcode: as[:number]}, []},
       hd(trimmed),
       {:pad, %{bytes: 2}, []}] # Ignore sequence number.
      ++ tl(trimmed)
    else
      [{:core_eventcode, %{bytes: 1, evcode: as[:number]}, []},
       hd(trimmed),
       {:pad, %{bytes: 2}, []}] # Ignore sequence number.
      ++ tl(trimmed)
    end

    quote do
      defmodule unquote(module_name) do
        defstruct unquote(Macro.escape([:usersent | as[:fields]]))
        unquote(generate_unit_functions(conts, mod_as[:modname], module_name))
      end
    end
  end

  def generate({:error, as, cs}, mod_as) do
    module_name = error_modname({mod_as[:modname], as[:name]})
    conts = [
      {:lit, %{bytes: 1, val: 0}, []},
      {:pad, %{bytes: 1}, []}, # Ignore error code
      {:pad, %{bytes: 2}, []}] # Ignore sequence number
     ++ trim_contents cs

    quote do
      defmodule unquote(module_name) do
        defstruct unquote(Macro.escape(as[:fields]))

        # Encoding functions are not generated for errors
        unquote(generate_decoding_fun(conts, mod_as[:modname], module_name))
      end
    end
  end

  def generate(unit, _) do
    raise "XEB.Proto: invalid protocol unit: #{inspect unit}"
  end

  #############################################################################
  # Helpers & utilities

  # Remove non-content elements from structured type contents
  def trim_contents(cs) do
    Enum.filter cs, fn
      {:reply, _, _} -> false
      {:doc, _, _} -> false
      _ -> true
    end
  end
end

