# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Proto.Fields do
  @moduledoc false

  import XEB.Proto.Modnames
  import Macro
  use Bitwise

  # This module deals with the generation of the code of 'structured units'
  # encoding/decoding functions. Structured units are all the units that are
  # defined as successions of fields, namely: structs, requests, replies,
  # events and errors.

  #############################################################################
  # Interface

  def generate_unit_functions(structure, namespace, mod) do
    quote do
      unquote(generate_encoding_fun(structure, namespace, mod))
      unquote(generate_decoding_fun(structure, namespace, mod))
    end
  end

  #############################################################################
  # Encoding function generation

  def generate_encoding_fun(conts, namespace, mod) do
    quote do
      def to_X(%unquote(mod){} = var!(packet), var!(exts) \\ %{}),
        do: unquote(generate_encoding_fun_body(conts, namespace, mod))
    end
  end

  defp generate_encoding_fun_body(conts, namespace, _mod, varcontext \\ :toplevel) do
    result = var(:result, varcontext)
    tacit = var(:tacit, varcontext)
    generated = var(:generated, varcontext)
    vars = {result, tacit, generated}
    extname = case Enum.find conts, &(elem(&1, 0) == :ext_opcode) do
      nil -> nil
      {:ext_opcode, %{ext: header}, []} -> header
    end

    prologue = quote do
      # The result being constructed
      unquote(result) = <<>>
      # Useful information that is not stored anywhere else
      unquote(tacit) = %{}
      # Keeping track of how many bytes were generated
      unquote(generated) = 0
    end

    body = Enum.reduce conts, prologue, fn (comp, acc) ->
      quote do
        _ = unquote(acc)
        unquote(component_StoX(comp, namespace, vars))
      end
    end

    length_setting = if Enum.any?(conts, &(elem(&1, 0) == :length_field)) do
      quote do
        %{at: at, len: len, unit: unit, base: base} = unquote(tacit)[:length_field]
        prefix = binary_part(unquote(result), 0, at)
        suffix = binary_part(unquote(result), at, byte_size(unquote(result)) - at)
        bitlen = len * 8
        len_val = (unquote(generated) + len - base)
        fld_val = if len_val <= 0 do 0 else round(len_val / unit) end
        align_len = if len_val <= 0 do 0 else (4 * fld_val) - len_val end
        align = String.duplicate(<<0>>, align_len)
        # Note that the 'generated' is not being updated because we assume
        # the addition of the length field is the last thing done before
        # returning the result
        # Also note that the length field is rounded up to the nearest
        # integer, in units of 4 bytes. In normal cases, it's not a problem
        # since the real length of the request is a multiple of 4 bytes
        # anyway, but if it isn't we need to add adequate padding to make
        # the real length of the request match the length field.
        prefix <> <<fld_val::unsigned-native-size(bitlen)>> <> suffix <> align
      end
    else
      quote do
        unquote(result)
      end
    end

    if extname do
      quote do
        if Enum.any?(var!(exts)[:opcodes], &(elem(&1, 0) == unquote(extname))) do
          # The unquoted expressions are assigned to _ in order to silence
          # 'ignored expression' warnings. It's ugly but it works
          _ = unquote(body)
          unquote(result) = unquote(length_setting)

          # These are just here to silence 'unused variable' warnings
          _ = unquote(generated)
          _ = var!(packet)
          _ = unquote(tacit)
          _ = var!(exts)

          unquote(result)
        else
          :unknown_ext
        end
      end
    else
      quote do
        # The unquoted expressions are assigned to _ in order to silence
        # 'ignored expression' warnings. It's ugly but it works
        _ = unquote(body)
        unquote(result) = unquote(length_setting)

        # These are just here to silence 'unused variable' warnings
        _ = unquote(generated)
        _ = var!(packet)
        _ = unquote(tacit)
        _ = var!(exts)

        unquote(result)
      end
    end
  end

  defp component_StoX({:field, %{type: ctype, name: cname} = as, []}, ns, {result, _, generated}) do
    cmodname = type_modname(ctype, ns)
    sem = semantic_fun_to_X(as, ns, &cmodname.to_X/1, &cmodname.raw_to_X/1)

    quote do
      fld_res = var!(packet)
      |> Map.fetch!(unquote(cname))
      |> unquote(sem).()

      unquote(result) = unquote(result) <> fld_res
      unquote(generated) = unquote(generated) + byte_size(fld_res)
    end
  end

  defp component_StoX({:exprfield, %{type: ctype, name: cname} = as, _}, ns, {result, _, generated}) do
    cmodname = type_modname(ctype, ns)
    sem = semantic_fun_to_X(as, ns, &cmodname.to_X/1, &cmodname.raw_to_X/1)

    quote do
      fld_res = var!(packet)
      |> Map.fetch!(unquote(cname))
      |> unquote(sem).()

      unquote(result) = unquote(result) <> fld_res
      unquote(generated) = unquote(generated) + byte_size(fld_res)
    end
  end

  defp component_StoX({:pad, %{bytes: bytes}, []}, _, {result, _, generated}) do
    quote do
      unquote(result) = unquote(result) <> String.duplicate(<<0>>, unquote(bytes))
      unquote(generated) = unquote(generated) + unquote(bytes)
    end
  end

  defp component_StoX({:pad, %{align: a}, []}, _, {result, _, generated}) do
    quote do
      bytes = rem(unquote(a) - rem(unquote(generated), unquote(a)), unquote(a))
      unquote(result) = unquote(result) <> String.duplicate(<<0>>, bytes)
      unquote(generated) = unquote(generated) + bytes
    end
  end

  defp component_StoX({:lit, %{bytes: bytes, val: val}, []}, _, {result, _, generated}) do
    quote do
      unquote(result) = unquote(result) <> <<unquote(val)::native-size(unquote(bytes * 8))>>
      unquote(generated) = unquote(generated) + unquote(bytes)
    end
  end

  defp component_StoX({:length_field, %{bytes: bytes, base: base}, []}, _, {_, tacit, generated}) do
    quote do
      unquote(tacit) = Map.put(unquote(tacit), :length_field,
        %{at: unquote(generated),
          len: unquote(bytes),
          unit: 4,
          base: unquote(base)})
    end
  end

  defp component_StoX({:core_eventcode, %{bytes: b, evcode: n}, []}, _, {result, _, generated}) do
    quote do
      code_val = unquote(n)
                 + if Map.fetch!(var!(packet), :usersent) == true do 128 else 0 end
      c = <<code_val::native-size(unquote(b * 8))>>
      unquote(result) = unquote(result) <> c
      unquote(generated) = unquote(generated) + unquote(b)
    end
  end

  defp component_StoX({:ext_eventcode, %{bytes: b, ext: h, evcode: n}, []}, _, {result, _, generated}) do
    quote do
      code_val = var!(exts)[unquote(h)][:fst_ev]
                 + unquote(n)
                 + if Map.fetch!(var!(packet), :usersent) == true do 128 else 0 end
      c = <<code_val::native-size(unquote(b * 8))>>
      unquote(result) = unquote(result) <> c
      unquote(generated) = unquote(generated) + unquote(b)
    end
  end

  defp component_StoX({:ext_opcode, %{bytes: b, ext: h}, []}, _, {result, _, generated}) do
    quote do
      c = <<var!(exts)[:opcodes][unquote(h)]::native-size(unquote(b * 8))>>
      unquote(result) = unquote(result) <> c
      unquote(generated) = unquote(generated) + unquote(b)
    end
  end

  defp component_StoX({:list, %{type: ctype, name: cname}, _}, ns, {result, _, generated}) do
    cmodname = type_modname(ctype, ns)

    quote do
      lst_res = var!(packet)
      |> Map.fetch!(unquote(cname))
      |> :"Elixir.XEB.Proto.Types.List".to_X(&unquote(cmodname).to_X/1)

      unquote(result) = unquote(result) <> lst_res
      unquote(generated) = unquote(generated) + byte_size(lst_res)
    end
  end

  defp component_StoX({:switch, _, [_ | cases]}, ns, {result, _, generated}) do
    switch = cases
    |> Enum.map(fn {_, _, [_ | fields]} -> fields end)
    |> Enum.reduce(<<>>, fn (fields, sw_acc) ->
      [flds_hd | flds_tl] = fields
      |> Enum.map(fn {_, as, _} -> as[:name] end)
      |> Enum.filter(&(&1 != nil))

      test = Enum.reduce(flds_tl,
        quote do (Map.fetch!(var!(packet), unquote(flds_hd)) != nil) end,
        fn (fld, test_acc) ->
          quote do: (unquote(test_acc) && (Map.fetch!(var!(packet), unquote(fld)) != nil))
        end)

      quote do
        unquote(sw_acc) <> if unquote(test) do
          unquote(generate_encoding_fun_body(fields, ns, :switch, :switch))
        else
          <<>>
        end
      end
    end)

    quote do
      sw_res = unquote(switch)
      unquote(result) = unquote(result) <> sw_res
      unquote(generated) = unquote(generated) + byte_size(sw_res)
    end
  end

  defp component_StoX(compt, namespace, _) do
    raise "StoX: #{inspect namespace}: component unsupported: #{inspect compt}"
  end


  #############################################################################
  # Decoding function generation
  
  # Two functions are generated here: one that returns the parsed data and the
  # remaining raw data that can then be threaded through the recursive-descent
  # parser, and one that simply returns the parsed data for external use.
  # The first is called do_from_X and the other is simply from_X.
  def generate_decoding_fun(conts, namespace, mod) do
    quote do
      def do_from_X(var!(data)) when is_binary(var!(data)),
        do: unquote(generate_decoding_fun_body(conts, namespace, mod))
      def from_X(data),
        do: data |> do_from_X |> elem(0)
    end
  end

  defp generate_decoding_fun_body(conts, namespace, mod) do
    prologue = quote do
      var!(result) = %unquote(mod){} # Actual result being constructed
      var!(tacit) = %{} # Information that is not in the result
      var!(consumed) = 0 # Keeping track of how many bytes have been consumed
    end

    body = Enum.reduce conts, prologue, fn (comp, acc) ->
      quote do
        # The unquoted expressions are assigned to _ in order to silence
        # 'ignored expression' warnings. It's ugly but it works
        _ = unquote(acc)
        unquote(component_SfromX(comp, namespace))
      end
    end

    quote do
      _ = unquote(body)

      # These are just here to silence 'unused variable' warnings
      _ = var!(consumed)

      {var!(result), var!(tacit), var!(data)}
    end
  end

  defp component_SfromX({:field, %{type: ctype, name: cname} = as, []}, namespace) do
    cmodname = type_modname(ctype, namespace)
    sem = semantic_fun_from_X(as, namespace, (quote do: &unquote(cmodname).do_from_X/1), cname)

    quote do
      {var!(result), var!(tacit), new_data} = unquote(sem).(var!(result), var!(tacit), var!(data))
      var!(consumed) = var!(consumed) + byte_size(var!(data)) - byte_size(new_data)
      var!(data) = new_data
    end
  end

  defp component_SfromX({:pad, %{bytes: bytes}, []}, _) do
    quote do
      <<_::native-size(unquote(bytes * 8))>> <> var!(data) = var!(data)
      var!(consumed) = var!(consumed) + unquote(bytes)
    end
  end

  defp component_SfromX({:pad, %{align: a}, []}, _) do
    quote do
      bytes = rem(unquote(a) - rem(var!(consumed), unquote(a)), unquote(a))
      bits = bytes * 8
      <<_::native-size(bits)>> <> var!(data) = var!(data)
      var!(consumed) = var!(consumed) + bytes
    end
  end

  defp component_SfromX({:lit, %{bytes: bytes, val: val}, []}, _) do
    quote do
      <<unquote(val)::size(unquote(bytes * 8))>> <> var!(data) = var!(data)
      var!(consumed) = var!(consumed) + unquote(bytes)
    end
  end

  defp component_SfromX({:length_field, %{bytes: bytes}, []}, _) do
    quote do
      <<sz::size(unquote(bytes * 8))>> <> var!(data) = var!(data)
      var!(tacit) = Map.put(var!(tacit), :length_field, sz)
      var!(consumed) = var!(consumed) + unquote(bytes)
    end
  end

  defp component_SfromX({:list, %{type: ctype, name: cname}, []}, namespace) do
    cmodname = type_modname(ctype, namespace)
    quote do
      {got_r, got_t, new_data} = :"Elixir.XEB.Proto.Types.List".do_from_X(
        var!(data), :entire_input,
        &unquote(cmodname).do_from_X/1)
      var!(result) = Map.put(var!(result), unquote(cname), got_r)
      var!(consumed) = var!(consumed) + byte_size(var!(data)) - byte_size(new_data)
      var!(data) = new_data
    end
  end

  defp component_SfromX({:list, %{type: ctype, name: cname}, [expr]}, namespace) do
    cmodname = type_modname(ctype, namespace)
    quote do
      len = unquote(expression_computation(expr))
      {got_r, got_t, new_data} = :"Elixir.XEB.Proto.Types.List".do_from_X(
        var!(data), len,
        &unquote(cmodname).do_from_X/1)
      var!(result) = Map.put(var!(result), unquote(cname), got_r)
      var!(consumed) = var!(consumed) + byte_size(var!(data)) - byte_size(new_data)
      var!(data) = new_data
    end
  end

  defp component_SfromX({:ext_eventcode, %{bytes: b}, []}, _) do
    quote do
      <<c::size(unquote(b * 8))>> <> var!(data) = var!(data)
      var!(result) = Map.put(var!(result), :usersent, c >= 128)
      var!(consumed) = var!(consumed) + unquote(b)
    end
  end

  defp component_SfromX({:core_eventcode, %{bytes: b}, []}, _) do
    quote do
      <<c::size(unquote(b * 8))>> <> var!(data) = var!(data)
      var!(result) = Map.put(var!(result), :usersent, c >= 128)
      var!(consumed) = var!(consumed) + unquote(b)
    end
  end

  defp component_SfromX(compt, namespace) do
    raise "SfromX: #{inspect namespace}: component unsupported: #{inspect compt}"
  end

  # Expressions in do_from_X
  defp expression_computation({:op, %{op: :"+"}, [opr1, opr2]}),
   do: quote do: unquote(expression_computation(opr1)) + unquote(expression_computation(opr2))
  defp expression_computation({:op, %{op: :"-"}, [opr1, opr2]}),
    do: quote do: unquote(expression_computation(opr1)) - unquote(expression_computation(opr2))
  defp expression_computation({:op, %{op: :"*"}, [opr1, opr2]}),
    do: quote do: unquote(expression_computation(opr1)) * unquote(expression_computation(opr2))
  defp expression_computation({:op, %{op: :"/"}, [opr1, opr2]}),
    do: quote do: div(unquote(expression_computation(opr1)), unquote(expression_computation(opr2)))
  defp expression_computation({:op, %{op: :"&"}, [opr1, opr2]}),
    do: quote do: unquote(expression_computation(opr1)) &&& unquote(expression_computation(opr2))
  defp expression_computation({:op, %{op: :"<<"}, [opr1, opr2]}),
    do: quote do: unquote(expression_computation(opr1)) <<< unquote(expression_computation(opr2))

  defp expression_computation({:unop, %{op: :"~"}, [opr]}),
    do: quote do: bnot(unquote(expression_computation(opr)))

  defp expression_computation({:fieldref, %{}, [:length]}),
    do: quote do: (Map.fetch!(var!(tacit), :length_field))
  defp expression_computation({:fieldref, %{}, [fld]}),
    do: quote do: (Map.fetch!(var!(result), unquote(fld)))

  defp expression_computation({:value, %{}, [val]}), do: val
  defp expression_computation({:bit, %{}, [bit]}),   do: 1 <<< bit
  #defp expression_computation({:enumref, %{ref: ref}, [enum]}), do:
  #defp expression_computation({:sumof, %{ref: ref}, []}), do:
  #defp expression_computation({:sumof, %{ref: ref}, [expr]}), do:
  #defp expression_computation({:popcount, %{}, [expr]}), do:
  #defp expression_computation({:"listelement-ref", %{}, []}), do:
  #defp expression_computation({:paramref, %{type: type}, [fld]}), do:

  defp expression_computation(exp),
    do: raise "Unsupported expression computation: #{inspect exp}"

  # Generation of code to parse enums/masks/altenums/altmasks
  defp semantic_fun_to_X(as, namespace, gen, rawgen) do
    cond do
      as[:altenum] || as[:altmask] ->
        emodname = type_modname(as[:altenum] || as[:altmask], namespace)
        quote do
          fn x ->
            alt = unquote(emodname).to_X(x)
            if alt do
              unquote(rawgen).(alt)
            else
              unquote(gen).(x)
            end
          end
        end

      as[:enum] || as[:mask] ->
        emodname = type_modname(as[:enum] || as[:mask], namespace)
        quote do
          fn x ->
            x
            |> unquote(emodname).to_X
            |> unquote(gen).()
          end
        end

      true ->
        gen
    end
  end

  defp semantic_fun_from_X(as, namespace, get, var) do
    cond do
      as[:altenum] || as[:altmask] ->
        emodname = type_modname(as[:altenum] || as[:altmask], namespace)
        quote do
          fn (r, t, d) -> # HACK: Maybe this could be done in a better way.
            {got_r, got_t, new_d} = unquote(get).(d)
            consumed_size = (byte_size(d) - byte_size(new_d)) * 8
            <<raw::native-size(consumed_size)>> <> new_d = d
            add_r = unquote(emodname).from_X(raw) || got_r
            {Map.put(r, unquote(var), add_r), Map.merge(t, got_t), new_d}
          end
        end

      as[:enum] || as[:mask] ->
        emodname = type_modname(as[:enum] || as[:mask], namespace)
        quote do
          fn (r, t, d) ->
            {got_r, got_t, new_d} = unquote(get).(d)
            add_r = unquote(emodname).from_X(got_r)
            {Map.put(r, unquote(var), add_r), Map.merge(t, got_t), new_d}
          end
        end

      true ->
        quote do
          fn (r, t, d) ->
            {got_r, got_t, new_d} = unquote(get).(d)
            {Map.put(r, unquote(var), got_r), Map.merge(t, got_t), new_d}
          end
        end
    end
  end
end

