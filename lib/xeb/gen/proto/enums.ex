# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Proto.Enums do
  @moduledoc false

  # Filtering and processing of enum items
  def item_value?({:item, _, [{:value, _, _}]}), do: true
  def item_value?(_), do: false

  def item_value({:item, %{name: n}, [{:value, %{}, [v]}]}), do: {n, v}

  def item_bit?({:item, _, [{:bit, _, _}]}), do: true
  def item_bit?(_), do: false

  def item_bit({:item, %{name: n}, [{:bit, %{}, [b]}]}), do: {n, b}

  # This checks if the last two items in the list have the same value
  # and merges them into a single one if needed.
  def merge_duplicate_items(curr, []), do: [curr]
  def merge_duplicate_items({_, c_val} = c, [{_, p_val} | _] = a) when p_val != c_val, do: [c | a]
  def merge_duplicate_items({curr_name, curr_val}, [{prev_name, _} | rest]) do
    new_name = String.to_atom(Atom.to_string(curr_name) <> "Or" <> Atom.to_string(prev_name))
    [{new_name, curr_val} | rest]
  end
end

