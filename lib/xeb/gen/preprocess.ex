# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Preprocess do
  @moduledoc false
  # This module process the raw description produced by XEB.Gen.Read into a
  # format suitable for code generation.

  #############################################################################
  # General functions

  defp charlist_to_atom(c) do
    c |> to_string |> String.to_atom
  end

  defp fields_list_request({:request, as, cs}) do
    new_cs = Enum.map cs, fn
      {:reply, _, _} = rep -> fields_list(rep)
      other -> other
    end

    fields_list({:request, as, new_cs})
  end

  defp fields_list({el_n, el_as, el_cs}) do
    list = el_cs |> Enum.map(fn
      {:field, as, _} -> as[:name]
      {:fd, as, _} -> as[:name]
      {:list, as, _} -> as[:name]
      {:valueparam, as, _} -> [as[:"value-mask-name"], as[:"value-list-name"]]
      {:switch, _, sw_cs} -> Enum.map sw_cs, fn # I'm sorry.
          {_, _, cs} -> Enum.map cs, fn
              {:field, as, _} ->as[:name]
              _ -> []
            end
        end
      _ -> []
    end) |> List.flatten

    {el_n, Map.merge(el_as, %{fields: list}), el_cs}
  end

  defp make_elem_copy({_copy_n, copy_as, _copy_cs}, orig_n, desc, modname) do
    {namespace, name} = case copy_as[:ref] do
      {ns, n} -> {ns, n}
      n -> {modname, n}
    end

    {orig_n, _orig_as, orig_cs} = desc
    |> Enum.find(fn {_, as, _} -> as[:modname] == namespace end)
    |> elem(2)
    |> Enum.find(fn {n, as, _} -> n == orig_n && as[:name] == name end)

    result_as = copy_as
    result_cs = orig_cs

    {orig_n, result_as, result_cs}
  end

  defp apply_copies(desc, copy_n, orig_n) do
    Enum.map desc, fn {:xcb, mod_as, mod_cs} ->
      new_mod_cs = Enum.map mod_cs, fn
        #{n, _, _} = el when n == copy_n -> make_elem_copy(el, orig_n, desc, mod_as[:modname])
        {^copy_n, _, _} = el -> make_elem_copy(el, orig_n, desc, mod_as[:modname])
        other -> other
      end
      {:xcb, mod_as, new_mod_cs}
    end
  end

  #############################################################################
  # For namespace resolution

  defp upcase_first(<<f::8>> <> rest) do
    String.upcase(<<f>>) <> rest
  end

  defp get_modname(mod_as) do
    if mod_as[:"extension-name"]
      do mod_as[:"extension-name"] |> Atom.to_string |> upcase_first
      else "Core"
    end
  end

  defp set_modname({:xcb, as, cs}) do
    new_as = Map.merge as, %{modname: get_modname(as)}
    {:xcb, new_as, cs}
  end

  defp type_in_namespace(type, imported_els, desc) do
    type_name = Atom.to_string type
    case String.split(type_name, ":") do
      [ns, n] ->
        {:xcb, ext_as, _} = Enum.find desc, fn {:xcb, as, _} ->
          as[:header] == charlist_to_atom(ns)
        end
        {ext_as[:modname], charlist_to_atom(n)}

      [_n] -> case imported_els[type] do
          nil ->
            type
          ns ->
            {:xcb, ext_as, _} = Enum.find desc, fn {:xcb, as, _} ->
              as[:header] == ns
            end
            {ext_as[:modname], type}
        end

      _ ->
        raise "Invalid type: #{inspect type}"
    end
  end

  defp field_types_in_namespace({fld_t, fld_as, fld_cs}, imported_els, desc) do
    attributes = [:type, :enum, :altenum, :mask, :altmask]
    new_as = Enum.reduce attributes, fld_as, fn(x, acc) ->
      if acc[x]
        do %{acc | x => type_in_namespace(acc[x], imported_els, desc)}
        else acc
      end
    end

    {fld_t, new_as, fld_cs}
  end

  defp xidunion_apply_namespace({:xidunion, as, cs}, imported_els, desc) do
    new_cs = Enum.map cs, fn {:type, %{}, [type]} ->
      new_type = type_in_namespace(type, imported_els, desc)
      {:type, %{}, [new_type]}
    end

    {:xidunion, as, new_cs}
  end

  defp req_apply_namespace({:request, as, cs}, imported_els, desc) do
    new_cs = Enum.map cs, fn
      {:reply, _, _} = rep -> apply_namespace(rep, imported_els, desc)
      other -> other
    end

    apply_namespace({:request, as, new_cs}, imported_els, desc)
  end

  defp apply_namespace({el_n, el_as, el_cs}, imported_els, desc) do
    new_cs = Enum.map el_cs, fn
      {:field, _, _} = fld -> field_types_in_namespace(fld, imported_els, desc)
      {:list, _, _} = fld -> field_types_in_namespace(fld, imported_els, desc)
      {:valueparam, _, _} = fld -> field_types_in_namespace(fld, imported_els, desc)
      {:switch, sw_as, sw_cs} ->
        new_sw_cs = Enum.map sw_cs, fn
          {c_n, c_as, c_cs} -> new_c_cs = Enum.map c_cs, fn
            {:field, _, _} = fld -> field_types_in_namespace(fld, imported_els, desc)
            other -> other
          end
          {c_n, c_as, new_c_cs}
        end
        {:switch, sw_as, new_sw_cs}
      other -> other
    end
    {el_n, el_as, new_cs}
  end

  defp copy_apply_namespace({el_n, el_as, el_cs}, imported_els, desc) do
    new_as = %{el_as | ref: type_in_namespace(el_as[:ref], imported_els, desc)}
    {el_n, new_as, el_cs}
  end

  defp ext_resolve_namespaces({:xcb, mod_as, mod_cs}, desc) do
    imports = Enum.filter_map mod_cs, &(elem(&1, 0) == :import), fn
      {:import, %{}, [header]} -> header
    end

    imported_els = desc
    |> Enum.filter(fn {:xcb, as, _} -> Enum.member?(imports, as[:header]) end)
    |> Enum.map(fn {:xcb, as, cs} -> Enum.map(cs, &({as[:header], &1})) end)
    |> List.flatten
    |> Enum.filter(&(elem(elem(&1, 1), 0) != :import))
    |> Enum.map(fn
      {ns, {_, %{name: n}, _}} -> {n, ns}
      {ns, {_, %{newname: n}, _}} -> {n, ns}
    end)

    new_cs = mod_cs
    |> Enum.filter(&(elem(&1, 0) != :import))
    |> Enum.map(fn
      {:xidunion, _, _} = el -> xidunion_apply_namespace(el, imported_els, desc)
      {:request, _, _} = el -> req_apply_namespace(el, imported_els, desc)
      {:struct, _, _} = el -> apply_namespace(el, imported_els, desc)
      {:event, _, _} = el -> apply_namespace(el, imported_els, desc)
      {:error, _, _} = el -> apply_namespace(el, imported_els, desc)
      {:eventcopy, _, _} = el -> copy_apply_namespace(el, imported_els, desc)
      {:errorcopy, _, _} = el -> copy_apply_namespace(el, imported_els, desc)
      other -> other
    end)

    {:xcb, mod_as, new_cs}
  end

  defp resolve_namespaces(desc) do
    Enum.map desc, &(ext_resolve_namespaces(&1, desc))
  end

  #############################################################################
  # For dependencies management

  # Returns a list of all the extensions the binding know about for the purpose
  # of the extension initialization in XEB.Server.
  def get_exts(raw_desc) do
    Enum.map raw_desc, fn {:xcb, as, _cs} ->
      %{header: as[:header],
        xname: Atom.to_string(as[:"extension-xname"]),
        # The only ext that doesn't have a 'name' is the core protocol
        name: as[:"extension-name"] || "Core"}
    end
  end

  # Return a list of all extensions (including the core protocol) and their
  # dependencies
  def get_deps(raw_desc) do
    Enum.map raw_desc, fn {:xcb, as, cs} ->
      header = as[:header]
      deps = if header == :xproto do
        []
      else
        ls = cs
        |> Enum.filter(&(elem(&1, 0) == :import))
        |> Enum.map(fn {:import, %{}, [e]} -> e end)
        [:xproto | ls]
      end

      {header, deps}
    end
  end

  #############################################################################
  # For requests isolation

  # Returns a list of namespaces and the requests they contain for the purpose
  # of generating direct functions (ie. one function per request, that issues
  # the matching request to the X server ; those function should live in the
  # XEB.Extension module)
  def get_reqs(raw_desc) do
    Enum.map raw_desc, fn {:xcb, as, cs} ->
      new_cs = Enum.filter_map(cs, &(elem(&1, 0) == :request), &fields_list/1)
      new_as = Map.merge(as, %{modname: get_modname(as)})
      {:xcb, new_as, new_cs}
    end
  end

  #############################################################################
  # For encoding/decoding purposes

  # Returns a list of namespaces and the structures they contain for the
  # purpose of generating the XEB.Proto module.
  def get_proto(raw_desc) do
    raw_desc
    |> Enum.map(&set_modname/1)
    |> resolve_namespaces
    |> apply_copies(:eventcopy, :event)
    |> apply_copies(:errorcopy, :error)
    |> Enum.map(fn {:xcb, mod_as, mod_cs} ->
      new_cs = Enum.map mod_cs, fn
        {:struct, _, _} = el -> fields_list(el)
        {:request, _, _} = el -> fields_list_request(el)
        {:error, _, _} = el -> fields_list(el)
        {:event, _, _} = el -> fields_list(el)
        other -> other
      end
      {:xcb, mod_as, new_cs}
    end)
  end
end
