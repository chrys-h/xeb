# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Gen.Proto do
  @moduledoc false

  import XEB.Gen.Utils

  # Protocol code generation happens on three levels:
  #  - Extension level or module level:
  #    Each submodule of XEB.Proto corresponds to an extension (and one submodule
  #    for the core protocol). For each extension, one module is generated. This
  #    level of the code generation is handled in this file.
  #  - Protocol unit level:
  #    We call a protocol unit any X11 packet definition, structure definition,
  #    enum definition, etc. Protocol units are the contents of extensions. Each
  #    protocol unit has a submodule in the module of the extension it is part
  #    of. The protocol unit level code generation is handled in XEB.Gen.Proto.Unit.
  #  - Field level:
  #    Certain protocol units (packets, structs) are made up of fields. In that
  #    sense, the fields are the contents of protocol units. Field-level code
  #    generation is handled in XEB.Gen.Proto.Fields. Certain type of fields are
  #    hardcoded into the XEB.Proto.Types module.

  def generate(raw_desc) do
    # Extension-level generation: extensions are iterated and generated.
    contents = raw_desc
    |> XEB.Gen.Preprocess.get_proto
    |> Enum.map(&generate_ext/1)
    |> flatten_quoted

    quote do
      defmodule :"Elixir.XEB.Proto" do
        @moduledoc false
        unquote(contents)
      end
    end
  end

  def generate_ext({:xcb, mod_as, mod_cs}) do
    modname = String.to_atom("Elixir.XEB.Proto." <> mod_as[:modname])
    contents = mod_cs
    |> Enum.map(&XEB.Gen.Proto.Unit.generate(&1, mod_as))
    |> flatten_quoted

    quote do
      defmodule unquote(modname) do
        @moduledoc false
        unquote(contents)
      end
    end
  end
end

