# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.ResID do
  @moduledoc ~S"""
  This module provides support for allocation and ownership of resource IDs (aka. XIDs).
  A process may be considered the 'owner' of an XID. In this case, the XEB server will notify the process any event or error regarding the XID through message passing. A given XID may be owned by multiple processes.
  A process becomes the owner of an XID in one of two ways:
  - By allocating a new XID with `id_alloc`. The newly allocated XID will be within the range given by the X server during connection setup, and it may be used as an ID for any atom, resource, window, etc. It can be released with `id_release`. It will be released automatically when the owner terminates.
  - By seizing ownership of an existing XID with `id_seize`. The XID may be any valid XID. This is useful, for instance to take ownership of the root window. The XID may be released with  `id_release` or is released automatically when the owner terminates.
  """

  use GenServer
  require Logger

  # Initial state for the GenServer and state variable template.
  @init_state %{first_xid: 0, last_xid: 0, next_xid: 0,
                valid_range: false, owners: %{}}

  ###################################################################
  # Client API

  @doc ~S"""
  Starts the server process.
  """
  def start_link do
    GenServer.start_link(__MODULE__, %{}, [])
  end

  def start_link(name) do
    GenServer.start_link(__MODULE__, %{}, [name: name])
  end

  @doc ~S"""
  Forces the server to internally change the XID range.
  """
  def set_range(resid_server \\ __MODULE__, mask, base) do
    GenServer.cast resid_server, {:set_range, mask, base}
  end

  @doc ~S"""
  Allocate and return a new XID within the current range. The function will fail if no XID could be allocated for whatever reason (currently no valid range set, ran out of available XIDs and could not get a new range, etc.)
  """
  def id_alloc(resid_server \\ __MODULE__) do
    case GenServer.call(resid_server, :alloc_xid) do
      :ran_out -> raise "XID allocation error: no more XIDs available"
      :no_range -> raise "XID allocation error: no valid range available"
      xid -> xid
    end
  end

  @doc ~S"""
  Release an XID owned by the caller process. If the XID is unknown or not owned by the caller process, nothing is done. Always returns :ok.
  """
  def id_release(resid_server \\ __MODULE__, xid) do
    GenServer.call(resid_server, {:release, xid})
  end

  @doc ~S"""
  Seize ownership of an XID that is not within the allocation range, i.e. the XID of a resource that belongs to another X11 client. Fails if the XID is already owned or is within the allocation range.
  """
  def id_seize(resid_server \\ __MODULE__, xid) do
    case GenServer.call(resid_server, {:seize, xid}) do
      :invalid_xid -> raise "Trying to seize an invalid XID"
      :ok -> :ok
    end
  end

  @doc ~S"""
  Returns a MapSet containing the PIDs of the owners of the specified XID, in no particular order. The MapSet may be empty if the XID is unknown or not owned by any process.
  """
  def who_has(resid_server \\ __MODULE__, xid) do
    GenServer.call(resid_server, {:who_has, xid})
  end

  @doc ~S"""
  Stops the server.
  """
  def stop(resid_server \\ __MODULE__) do
    GenServer.stop(resid_server)
  end

  ###################################################################
  # GenServer callbacks

  def init(%{}) do
    Process.flag(:trap_exit, true)
    {:ok, @init_state}
  end

  def handle_call(:alloc_xid, {from, _}, %{last_xid: l, next_xid: n, valid_range: v} = state) do
    cond do
      not v -> {:reply, :no_range, state}
      n == l -> {:reply, :ran_out, state}
      true -> {:reply, n, %{own_xid(n, from, state) | next_xid: n + 1}}
    end
  end

  def handle_call({:release, xid}, {from, _}, state) do
    owners = owners(xid, state)
    if MapSet.member?(owners, from) do
      {:reply, :ok, disown_xid(xid, from, state)}
    else
      Logger.warn "Process #{inspect from} tried to release XID #{inspect xid}, which is unknown or not owned by the caller process."
      {:reply, :ok, state}
    end
  end

  def handle_call({:who_has, xid}, _, state) do
    {:reply, owners(xid, state), state}
  end

  def handle_call({:seize, xid}, {from, _}, state) do
    if valid_xid?(xid)
      do {:reply, :ok, own_xid(xid, from, state)}
      else {:reply, :invalid_xid, state}
    end
  end

  def handle_cast({:set_range, mask, base}, state) do
    new_state = %{state |
      first_xid: base,
      last_xid: mask + base,
      next_xid: base,
      valid_range: true
    }
    {:noreply, new_state}
  end

  def handle_info({:EXIT, from, _}, state) do
    # A process owning XIDs has died, so we rewoke his ownership of the
    # XIDs. As in UNIX, process management is cruel...
    new_state = Enum.reduce all_owned_by(from, state), state, fn (xid, st) ->
      disown_xid(xid, from, st)
    end
    {:noreply, new_state}
  end

  def terminate(_reason, %{owners: o} = _state) do
    Enum.map o, fn {_, os} -> Enum.map os, &Process.unlink/1 end
  end

  ###################################################################
  # Backend functions

  defp valid_xid?(i) when is_integer(i)
                      and i >= 0
                      and i < 0x40000000, do: true
  defp valid_xid?(_), do: false
  defp owners(xid, %{owners: o}), do: Map.get o, xid, MapSet.new()

  defp all_owned_by(pid, %{owners: o}) do
    Enum.filter_map o, fn {_, os} -> MapSet.member?(os, pid) end, fn {xid, _} -> xid end
  end

  defp own_xid(xid, pid, %{owners: o} = state) do
    new_xid_owners = xid |> owners(state) |> MapSet.put(pid)
    new_o = Map.put o, xid, new_xid_owners
    Process.link pid
    %{state | owners: new_o}
  end

  defp disown_xid(xid, pid, %{owners: o} = state) do
    new_xid_owners = xid |> owners(state) |> MapSet.delete(pid)
    new_o = if Enum.empty?(new_xid_owners)
      do Map.delete o, xid
      else Map.put o, xid, new_xid_owners
    end
    Process.unlink pid
    %{state | owners: new_o}
  end
end

