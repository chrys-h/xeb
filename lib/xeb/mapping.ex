# Copyright (c) 2017, Chrysostome Huchet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without  limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the  Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

defmodule XEB.Mapping do
  @moduledoc ~S"""
  Provides a process that keeps track of the keyboard and pointer mappings of the server.
  """

  # HACK - TODO: Clarify the keysyms vs keycodes situation

  require XEB
  use GenServer
  require Logger

  # Elements of the ModMask mask
  @modmask_elems [:Shift, :Lock, :Control, :"1", :"2", :"3", :"4", :"5", :Any]

  # Initial state value and template
  @init_state %{
    keycode_range: nil,
    pointer_mapping: [],
    # For modifiers, a keyword list is more convenient because
    # there are typically few (less than 20) modifier keys, and
    # it makes iteration nicer.
    # The tuples are {<ModMask element>, <KEYCODE>}
    modifiers: [],
    # The keyboard mapping is kept in two separate maps:
    # one that matches keycodes to their goups of keysyms, and
    # one that matches keysyms to their keycodes. Each keysym
    # is assumed to correspond to only one keycode.
    keycodes: %{}, # Keysysms => Keycodes
    keysyms: %{} # Keycodes => Keysyms
  }

  #############################################################################
  # Client API

  @doc ~S"""
  Starts the process, queries current mappings.
  """
  def start_link(name \\ __MODULE__) do
    GenServer.start_link(__MODULE__, %{}, [name: name])
  end

  @doc ~S"""
  Trigger an update of the mapping of the specified component. The component specification is the same as the 'Mapping' enum of the core protocol, meaning that it can take values `:Modifier`, `:Keyboard`, `:Pointer`.
  """
  def update_mapping(svr \\ __MODULE__, component) do
    GenServer.cast svr, {:update, component}
  end

  @doc ~S"""
  Get a list of the keysyms corresponding to a given keycode, or `:none` if the keycode is unknown.
  """
  def lookup_keycode(svr \\ __MODULE__, code) do
    GenServer.call svr, {:lookup_keycode, code}
  end

  @doc ~S"""
  Get the keycode corresponding to a given keysym, or `:none` if the keysym is unknown.
  """
  def lookup_keysym(svr \\ __MODULE__, sym) do
    Logger.debug "XEB.Mapping.lookup_keysym: #{inspect sym}"
    symbol = if is_atom(sym) do
      XEB.Keysyms.lookup_name(sym)
    else
      sym
    end
    Logger.debug "     ...i.e. #{inspect symbol}"
    res = GenServer.call svr, {:lookup_keysym, symbol}
    Logger.debug "     ...i.e. #{inspect res}"
    res
  end

  @doc ~S"""
  Get the ModMask element corresponding to a given key. The key can be a KEYSYM (or an atom representing a KEYSYM as recognized by XEB.Keysyms), a KEYCODE, or a ModMask element (as an atom). The return value is either the ModMask element the key is mapped to if any, or nil otherwise.
  """
  def lookup_mod_reverse(svr \\ __MODULE__, sym) do
    GenServer.call svr, {:lookup_mod_reverse, sym}
  end

  @doc ~S"""
  Convert a ModMask specification to a KeyButMask specification.
  """
  def to_KeyButMask(:"1"), do: :Mod1
  def to_KeyButMask(:"2"), do: :Mod2
  def to_KeyButMask(:"3"), do: :Mod3
  def to_KeyButMask(:"4"), do: :Mod4
  def to_KeyButMask(:"5"), do: :Mod5
  def to_KeyButMask(:Any), do: raise ":Any cannot be converted to KeyButMask"
  def to_KeyButMask(atom) when is_atom(atom), do: atom
  def to_KeyButMask(mask), do: mask |> Enum.map(&to_KeyButMask/1) |> MapSet.new

  @doc ~S"""
  Convert a KeyButMask specification to a ModMask specification.
  """
  def to_ModMask(:Mod1), do: :"1"
  def to_ModMask(:Mod2), do: :"1"
  def to_ModMask(:Mod3), do: :"1"
  def to_ModMask(:Mod4), do: :"1"
  def to_ModMask(:Mod5), do: :"1"
  def to_ModMask(:Button1), do: raise "Button1 cannot be converted to ModMask"
  def to_ModMask(:Button2), do: raise "Button2 cannot be converted to ModMask"
  def to_ModMask(:Button3), do: raise "Button3 cannot be converted to ModMask"
  def to_ModMask(:Button4), do: raise "Button4 cannot be converted to ModMask"
  def to_ModMask(:Button5), do: raise "Button5 cannot be converted to ModMask"
  def to_ModMask(atom) when is_atom(atom), do: atom
  def to_ModMask(mask), do: mask |> Enum.map(&to_ModMask/1) |> MapSet.new

  #############################################################################
  # GenServer callbacks

  def init(%{}) do
    state = %{@init_state | keycode_range: XEB.Server.get_keycode_range()}
    state = get_all_mappings state
    {:ok, state}
  end

  def handle_call({:lookup_keysym, sym}, _, %{keycodes: codes} = state) do
    res = Map.get codes, sym, :none
    {:reply, res, state}
  end

  def handle_call({:lookup_keycode, code}, _, %{keysyms: syms} = state) do
    res = Map.get syms, code, []
    {:reply, res, state}
  end

  def handle_call({:lookup_mod_reverse, key}, _, state) do
    {:reply, do_lookup_mod_reverse(key, state), state}
  end

  def handle_cast({:update, :Modifier}, state), do: {:noreply, get_modifiers(state)}
  def handle_cast({:update, :Pointer}, state),  do: {:noreply, get_pointer_mapping(state)}
  def handle_cast({:update, :Keyboard}, state), do: {:noreply, get_keyboard_mapping(state)}
  def handle_cast({:update, component}, state) do
    Logger.warn "Trying to update the mapping of invalid component #{inspect component}"
    {:noreply, state}
  end

  #############################################################################
  # Backend functions

  defp do_lookup_mod_reverse(%XEB.Proto.Core.KEYCODE{} = code, %{modifiers: mods} = state) do
    mods
    |> Enum.find(fn {mod, codes} -> code in codes end)
    |> case do
      {mod, _} -> mod
      nil -> nil
    end
  end

  defp do_lookup_mod_reverse(%XEB.Proto.Core.KEYSYM{} = sym, %{keycodes: codes} = state) do
    case Map.get(codes, sym) do
      nil -> nil
      code -> do_lookup_mod_reverse(code, state)
    end
  end

  defp do_lookup_mod_reverse(:Shift,   _), do: :Shift
  defp do_lookup_mod_reverse(:Lock,    _), do: :Lock
  defp do_lookup_mod_reverse(:Control, _), do: :Control
  defp do_lookup_mod_reverse(:Any,     _), do: :Any
  # We use :ModX here because the atoms made up of the number itself
  # collide with keysym names. Right now, it makes sense because this
  # function is used for user-friendliness (in declarations of hotkeys)
  # but it may cause bugs in the future #HACK
  defp do_lookup_mod_reverse(:Mod1, _), do: :"1"
  defp do_lookup_mod_reverse(:Mod2, _), do: :"2"
  defp do_lookup_mod_reverse(:Mod3, _), do: :"3"
  defp do_lookup_mod_reverse(:Mod4, _), do: :"4"
  defp do_lookup_mod_reverse(:Mod5, _), do: :"5"

  defp do_lookup_mod_reverse(sym, state) do
    case XEB.Keysyms.lookup_name(sym) do
      nil -> nil
      ks -> do_lookup_mod_reverse(ks, state)
    end
  end

  defp get_all_mappings(state) do
    state
    |> get_keyboard_mapping
    |> get_modifiers
    |> get_pointer_mapping
  end

  defp get_keyboard_mapping(%{keycode_range: {fst, lst}} = state) do
    count = lst.val - fst.val + 1
    %XEB.Proto.Core.GetKeyboardMapping.Reply{
      keysyms_per_keycode: n_per_code,
      keysyms: kss
    } = XEB.Core.get_keyboard_mapping first_keycode: fst, count: count

    keysyms_map = kss
    |> split_map(fst.val, n_per_code)
    |> Enum.map(fn {kcn, kss} -> {%XEB.Proto.Core.KEYCODE{val: kcn}, kss} end)

    keycodes_map = keysyms_map
    |> Enum.map(fn {kcn, kss} -> Enum.map kss, &({&1, kcn}) end)
    |> List.flatten

    %{state |
      keycodes: Map.new(keycodes_map),
      keysyms: Map.new(keysyms_map)
    }
  end

  defp get_modifiers(state) do
    %XEB.Proto.Core.GetModifierMapping.Reply{
      keycodes_per_modifier: n_per_mod,
      keycodes: kcs
    } = XEB.Core.get_modifier_mapping()

    names = @modmask_elems
    new_mods = split_map(kcs, 0, n_per_mod)
    |> Enum.map(fn {k, v} -> {Enum.fetch!(names, k), v} end)

    %{state | modifiers: new_mods}
  end

  defp get_pointer_mapping(state) do
    %XEB.Proto.Core.GetPointerMapping.Reply{
      map_len: _,
      map: map
    } = XEB.Core.get_pointer_mapping()
    %{state | pointer_mapping: map}
  end

  # Turn a list into a map that matches groups with their number.
  defp split_map(list, fst, group_size) do
    chunks = Enum.chunk list, group_size
    last = fst + length(chunks) - 1
    labeled = Enum.zip fst..last, chunks
    Map.new labeled
  end
end

