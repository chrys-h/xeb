# The contents of this file are partially based on the X11 protocol library.

# latin1.ks - definition of Latin-1 keysyms. Those keysyms are
# the ones collected into the "LATIN-1" set in the X11 Protocol reference
# (see https://www.x.org/docs/XProtocol/proto.pdf pages 102 to 105), or the
# XK_LATIN1 section in /usr/include/X11/keysymdef.h (or equivalent)
# A number of those characters have been written off because I do not
# intend to use them in WM hotkeys. The most notable victims are letters
# with diacritics, brackets and other special characters.

# keysym number		keysym name
0x0020                  Space          # U+0020 SPACE
0x0021                  Exclam         # U+0021 EXCLAMATION MARK
0x0022                  Quotedbl       # U+0022 QUOTATION MARK
0x0023                  Numbersign     # U+0023 NUMBER SIGN
0x0026                  Ampersand      # U+0026 AMPERSAND
0x0027                  Apostrophe     # U+0027 APOSTROPHE
0x002a                  Asterisk       # U+002A ASTERISK
0x002b                  Plus           # U+002B PLUS SIGN
0x002c                  Comma          # U+002C COMMA
0x002d                  Minus          # U+002D HYPHEN-MINUS
0x002e                  Period         # U+002E FULL STOP
0x002f                  Slash          # U+002F SOLIDUS
0x0030                  0              # U+0030 DIGIT ZERO
0x0031                  1              # U+0031 DIGIT ONE
0x0032                  2              # U+0032 DIGIT TWO
0x0033                  3              # U+0033 DIGIT THREE
0x0034                  4              # U+0034 DIGIT FOUR
0x0035                  5              # U+0035 DIGIT FIVE
0x0036                  6              # U+0036 DIGIT SIX
0x0037                  7              # U+0037 DIGIT SEVEN
0x0038                  8              # U+0038 DIGIT EIGHT
0x0039                  9              # U+0039 DIGIT NINE
0x003a                  Colon          # U+003A COLON
0x003b                  Semicolon      # U+003B SEMICOLON
0x003c                  Less           # U+003C LESS-THAN SIGN
0x003d                  Equal          # U+003D EQUALS SIGN
0x003e                  Greater        # U+003E GREATER-THAN SIGN
0x003f                  Question       # U+003F QUESTION MARK
0x0040                  at             # U+0040 COMMERCIAL AT
0x0041                  A              # U+0041 LATIN CAPITAL LETTER A
0x0042                  B              # U+0042 LATIN CAPITAL LETTER B
0x0043                  C              # U+0043 LATIN CAPITAL LETTER C
0x0044                  D              # U+0044 LATIN CAPITAL LETTER D
0x0045                  E              # U+0045 LATIN CAPITAL LETTER E
0x0046                  F              # U+0046 LATIN CAPITAL LETTER F
0x0047                  G              # U+0047 LATIN CAPITAL LETTER G
0x0048                  H              # U+0048 LATIN CAPITAL LETTER H
0x0049                  I              # U+0049 LATIN CAPITAL LETTER I
0x004a                  J              # U+004A LATIN CAPITAL LETTER J
0x004b                  K              # U+004B LATIN CAPITAL LETTER K
0x004c                  L              # U+004C LATIN CAPITAL LETTER L
0x004d                  M              # U+004D LATIN CAPITAL LETTER M
0x004e                  N              # U+004E LATIN CAPITAL LETTER N
0x004f                  O              # U+004F LATIN CAPITAL LETTER O
0x0050                  P              # U+0050 LATIN CAPITAL LETTER P
0x0051                  Q              # U+0051 LATIN CAPITAL LETTER Q
0x0052                  R              # U+0052 LATIN CAPITAL LETTER R
0x0053                  S              # U+0053 LATIN CAPITAL LETTER S
0x0054                  T              # U+0054 LATIN CAPITAL LETTER T
0x0055                  U              # U+0055 LATIN CAPITAL LETTER U
0x0056                  V              # U+0056 LATIN CAPITAL LETTER V
0x0057                  W              # U+0057 LATIN CAPITAL LETTER W
0x0058                  X              # U+0058 LATIN CAPITAL LETTER X
0x0059                  Y              # U+0059 LATIN CAPITAL LETTER Y
0x005a                  Z              # U+005A LATIN CAPITAL LETTER Z
0x0061                  a              # U+0061 LATIN SMALL LETTER A
0x0062                  b              # U+0062 LATIN SMALL LETTER B
0x0063                  c              # U+0063 LATIN SMALL LETTER C
0x0064                  d              # U+0064 LATIN SMALL LETTER D
0x0065                  e              # U+0065 LATIN SMALL LETTER E
0x0066                  f              # U+0066 LATIN SMALL LETTER F
0x0067                  g              # U+0067 LATIN SMALL LETTER G
0x0068                  h              # U+0068 LATIN SMALL LETTER H
0x0069                  i              # U+0069 LATIN SMALL LETTER I
0x006a                  j              # U+006A LATIN SMALL LETTER J
0x006b                  k              # U+006B LATIN SMALL LETTER K
0x006c                  l              # U+006C LATIN SMALL LETTER L
0x006d                  m              # U+006D LATIN SMALL LETTER M
0x006e                  n              # U+006E LATIN SMALL LETTER N
0x006f                  o              # U+006F LATIN SMALL LETTER O
0x0070                  p              # U+0070 LATIN SMALL LETTER P
0x0071                  q              # U+0071 LATIN SMALL LETTER Q
0x0072                  r              # U+0072 LATIN SMALL LETTER R
0x0073                  s              # U+0073 LATIN SMALL LETTER S
0x0074                  t              # U+0074 LATIN SMALL LETTER T
0x0075                  u              # U+0075 LATIN SMALL LETTER U
0x0076                  v              # U+0076 LATIN SMALL LETTER V
0x0077                  w              # U+0077 LATIN SMALL LETTER W
0x0078                  x              # U+0078 LATIN SMALL LETTER X
0x0079                  y              # U+0079 LATIN SMALL LETTER Y
0x007a                  z              # U+007A LATIN SMALL LETTER Z


