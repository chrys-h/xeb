# The contents of this file are partially based on the X11 protocol library.

# common.ks - definition of common and universal keysyms. Those keysyms are
# the ones collected into the "keyboard" set in the X11 Protocol reference
# (see https://www.x.org/docs/XProtocol/proto.pdf pages 102 to 105), or the
# XK_MISCEALLANY section in /usr/include/X11/keysymdef.h (or equivalent)
# By definifion, the first two digits of all these symbols' numbers are FF.
# Certain sections have been written off because I don't intend to use them
# for window manager hotkeys. The keypad is the most remarkable victim.

# keysym number		keysym name
0x000000                NoSymbol    # NoSymbol - See protocol reference
0xffffff                VoidSymbol  # VoidSymbol - See protocol reference

0xff08                  BackSpace
0xff08                  Backspace
0xff09                  Tab
0xff0a                  Linefeed
0xff0b                  Clear
0xff0d                  Return
0xff0d                  Enter
0xff13                  Pause
0xff14                  Scroll_Lock
0xff15                  Sys_Req
0xff1b                  Escape
0xffff                  Delete

0xff50                  Home
0xff51                  Left
0xff52                  Up
0xff53                  Right
0xff54                  Down
0xff55                  Prior
0xff55                  Page_Up
0xff56                  Next
0xff56                  Page_Down
0xff57                  End
0xff58                  Begin

0xff60                  Select
0xff61                  Print
0xff62                  Execute
0xff63                  Insert
0xff65                  Undo
0xff66                  Redo
0xff67                  Menu
0xff68                  Find
0xff69                  Cancel
0xff6a                  Help
0xff6b                  Break
0xff7e                  Mode_switch
0xff7e                  Script_switch
0xff7f                  Num_Lock

0xffbe                  F1
0xffbf                  F2
0xffc0                  F3
0xffc1                  F4
0xffc2                  F5
0xffc3                  F6
0xffc4                  F7
0xffc5                  F8
0xffc6                  F9
0xffc7                  F10
0xffc8                  F11
0xffc9                  F12
0xffca                  F13
0xffcb                  F14
0xffcc                  F15

0xffe1                  Shift_L
0xffe2                  Shift_R
0xffe3                  Control_L
0xffe4                  Control_R
0xffe5                  Caps_Lock
0xffe6                  Shift_Lock

0xffe7                  Meta_L
0xffe8                  Meta_R
0xffe9                  Alt_L
0xffea                  Alt_R
0xffeb                  Super_L
0xffec                  Super_R
0xffed                  Hyper_L
0xffee                  Hyper_R



