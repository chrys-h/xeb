# XEB

XEB aims at being a pure-Elixir and idiomatic low-level binding to the X11 protocol.

## Installation

As of now, XEB is not available in Hex. To use XEB in your project, the GitHub repo needs to be added as a dependency in your project's `mix.exs`:
```elixir
def deps do
  [{:xeb, github: "chrys-h/XEB", submodules: true}]
end
```

### Running tests

To compile XEB form this directory, it is important to update the submodules with
```
git submodule init
git submodule update
```

The unit tests may require a running X server. If you want or need to avoid interfering with your normal X server, [Xephyr](https://www.freedesktop.org/wiki/Software/Xephyr/) can be used as an alternative.
The tests can then be run with:
```
mix test
```

## Usage

## Licence

The entirety of the code is under MIT licence.

## TODO

- [ ] Support for extensions in `XEB.Server` (querying during init, threading the `exts` variable, etc.)
- [X] Make an actual OTP application
- [X] Better arguments for `XEB` functions
  - [X] Automatically compute value masks
- [ ] Support for XInput, RandR, Xkb, GLX, SHM, DRI3, Present. See `@unsupported` in `XEB.Gen`
- [X] Rename modules (e.g. X11 -> XEB)
- [X] Make it possible to choose at compile-time which extensions will be compiled (in config.exs)
  - [X] Dependency graph of extensions
  - [ ] Same for keysyms
- [ ] Figure out which extensions are deprecated and/or effectively useless and can be dropped.
- [X] Refactor `XEB.Proto` and make the code clearer.
  - [X] Centralize code generation and make it depend on which extensions are chosen and the resolution of their dependencies first of all.
    - [X] `XEB.Gen` module?
  - [ ] Use more precise vocabulary (eg. "encode"/"decode" instead of "translate")
  - [x] The `AtomUtils` module is an abomination and its existence needs to stop.
  - [X] So is `XEB.Proto.Helpers`.
  - [X] Move `XEB.Proto.Read`, `XEB.Proto.Modnames` and `XEB.Proto.Helpers` out of `XEB.Proto`
- [ ] Write tests for `XEB.Server`.
- [ ] Typespecs and Dialyzer
- [ ] Documentation
- [ ] Write higher-level components for drawing, UI, etc.?
- [ ] `git grep TODO`
- [ ] `git grep HACK`
- [ ] Fix the handling of switches. So far, only a few special (but common) cases are handled.
- [ ] Compilation takes several minutes ; fix that.

